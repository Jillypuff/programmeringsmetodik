/**
 * 
 */
package kap5;

/**
 * Create an array of int to store in ManipulateArray
 * and return different versions of it.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-26
 */
public class P5_3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ManipulateArray ma = new ManipulateArray();
		
		System.out.println(ma.swapFirstAndLast());
		System.out.println(ma.rightShiftOneStep());
		for(int i : ma.getArrayCopy())
			System.out.print(i + " ");
		System.out.println("");
		for(int i : ma.replaceWithBiggestNeighbor())
			System.out.print(i + " ");
		System.out.println("");

	}

}
