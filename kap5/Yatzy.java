package kap5;

/**
 * Yatzy without the game and you can only roll dices.
 * Select amount of dices when constructing the class.
 * Roll them then check either total value of all the dices 
 * in string form.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-27
 */

public class Yatzy {
	Dice[] dice;
	
	public Yatzy() {
		dice = new Dice[5];
		for(int i = 0; i < dice.length; i++)
			dice[i] = new Dice();
	}
	public Yatzy(int nbrOfDice) {
		dice = new Dice[nbrOfDice];
		for(int i = 0; i < dice.length; i++) {
			dice[i] = new Dice();
		}
	}
	
	public void roll() {
		for(Dice d : dice) {
			d.roll();
		}
	}
	public int getTotalValue() {
		int sum = 0;
		for(Dice d : dice)
			sum += d.getValue();
		return sum;
	}
	public String toString() {
		String returnString = "";
		for(Dice d : dice)
			returnString += d.toString();
		return returnString;
	}
}
