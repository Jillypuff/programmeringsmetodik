/**
 * 
 */
package kap5;

/**
 * Checks if numbers in an array have different attributes
 * such as ascending and descending and others.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-06
 */
public class P5_2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		CheckNumberArray cna = new CheckNumberArray();
		cna.addNbr(8);
		cna.addNbr(6);
		cna.addNbr(4);
		cna.addNbr(9);
		cna.addNbr(-1);
		cna.addNbr(8);
		cna.addNbr(6);
		cna.addNbr(4);
		cna.addNbr(9);
		cna.addNbr(-1);
		cna.addNbr(5000000);

//		for(int i = 0; i < 10; i++)
//			cna.addNbr(i);
		System.out.println(cna.getSum());
		System.out.println(cna.getAverage());
		System.out.println(cna.isAccOrder());
		System.out.println("Dec?:" + cna.isDesOrder());
		System.out.println(cna.isOrdered());
	}

}
