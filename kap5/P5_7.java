/**
 * 
 */
package kap5;

/**
 * Stores a student and his/her quiz score by taking in values
 * through arguments when code is ran.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-26
 */

public class P5_7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if(args.length == 0) {
			System.out.println("Add arguments before running.");
			System.exit(0);
		}
			
		Student2 s2 = new Student2(args[0]);
		
		for(int i = 1; i < args.length; i++)
			s2.addQuizScore(Double.valueOf(Double.valueOf(args[i])));
		
		System.out.println(s2.getName());
		System.out.println(s2.getNbrOfQuiz());
		System.out.println(s2.getTotalScore());
		System.out.println(s2.getAverageScore());
		
		System.exit(0);
	}
}
