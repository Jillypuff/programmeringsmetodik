/**
 * 
 */
package kap5;

import java.util.Random;

/**
 * Stores a student and his/her quiz scores.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-26
 */
public class P5_6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Student2 s2 = new Student2("Jake");
		
		Random rand = new Random();
		for(int i = 0; i < 10; i++)
			s2.addQuizScore(rand.nextInt(100));
		System.out.println(s2.getAverageScore());
		System.out.println(s2.getAverageScore());
		System.out.println(s2.getAverageScore());
		System.out.println(s2.toString());
		for(double d : s2.getQuizzesCopy())
			System.out.println(d);

		
	}

}
