/**
 * 
 */
package kap5;

//import java.util.Random;

/**
 * Single matrix of a Sudoku
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-27
 */
public class P5_10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		SudokuSquare ss = new SudokuSquare();
		System.out.println(ss.getValue(0, 0));
// 		Random rand = new Random();
//		int value;
//		int row;
//		int column;
//		int tries = 0;
//		while(!ss.isValidSquare()) {
//			value = rand.nextInt(1,10);
//			row = rand.nextInt(0,3);
//			column = rand.nextInt(0,3);
//			ss.setValue(value, row, column);
//			System.out.println(ss.toString());
//			tries++;
//		}
//		System.out.println(ss.getValue(1, 1));
//		System.out.println(ss.getValue(0, 1));
//		System.out.println(ss.getValue(2, 2));
//		System.out.format("Amount of tries to complete: %d", tries);
		
		System.exit(0);
	}
}
