package kap5;

import java.util.Scanner;

/**
 * Store a string and return different versions of it
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-26
 */

public class CheckString2 {
	String str;
	
	public CheckString2(String str) {
		this.str = str;
	}
	
	public String getString() {
		return str;
	}
	public String middle() {
		if(str.length() == 0) return "";
		int middle = str.length() / 2;
		if(str.length() % 2 == 0)
			return str.substring(middle-1, middle+1);
		else
			return str.substring(middle, middle+1);
	}
	public int countVowels() {
		String vowels = "AOUEÅÄÖYIaoueåäöyi";
		int totalVowels = 0;
		for(int i = 0; i < str.length(); i++) {
			if(vowels.contains(Character.toString(str.charAt(i)))) {
				totalVowels++;
			}
		}
		return totalVowels;
	}
	public int findString(String otherStr) {
		if(str.length() == 0)
			return -1;
		if(otherStr.length() == 0)
			return 0;
		for(int i = 0; i < str.length(); i++) {
			if(str.length() - i < otherStr.length())
				return -1;
			if(otherStr.charAt(0) == str.charAt(i)) {
				int temp = 0;
				while(otherStr.charAt(0+temp) == str.charAt(i+temp)) {
					if(temp == otherStr.length() - 1)
						return i;
					else
						temp++;
				} 
			}
		}
		return -1;
	}
	public int countWords() {
		Scanner wordCounter = new Scanner(str);
		int totalWords = 0;
		while(wordCounter.hasNext()) {
			totalWords++;
			wordCounter.next();
		}
		wordCounter.close();
		return totalWords;
	}
}
