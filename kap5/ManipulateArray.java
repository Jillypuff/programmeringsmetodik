package kap5;

/**
 * Class that stores an array of ints and manipulates
 * it in different way with methods then return the new results.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-26
 */

public class ManipulateArray {
	int[] arrayOfInt = new int[10];
	//int[] arrayOfInt = {5,4,0,3,9,2,4};
	
	public ManipulateArray() {
		
	}
	
	public ManipulateArray(int arr[]) {
		arrayOfInt = arr;
	}
	
	public int[] getArrayCopy() {
		return arrayOfInt.clone();
	}
	public int getArraySize() {
		return arrayOfInt.length;
	}
	public String swapFirstAndLast() {
		int[] tempArray = arrayOfInt.clone();
		int temp = tempArray[0];
		tempArray[0] = tempArray[tempArray.length - 1];
		tempArray[tempArray.length - 1] = temp;
		String returnString = "[";
		for(int i = 0; i < tempArray.length - 1; i++)
			returnString += tempArray[i] + ",";
		returnString += tempArray[tempArray.length - 1] + "]";
		return returnString;
	}
	public String rightShiftOneStep() {
		int[] tempArray = arrayOfInt.clone();
		int temp = tempArray[tempArray.length - 1];
		for(int i = 1; i < tempArray.length; i++)
			tempArray[tempArray.length - i] = tempArray[tempArray.length - i -1];
		tempArray[0] = temp;
		String returnString = "[";
		for(int i = 0; i < tempArray.length - 1; i++)
			returnString += tempArray[i] + ",";
		returnString += tempArray[tempArray.length - 1] + "]";
		return returnString;
	}
	public int[] replaceWithBiggestNeighbor() {
		int[] tempArray = arrayOfInt.clone();
		for(int i = 1; i < tempArray.length - 1; i++)
			tempArray[i] = Math.max(arrayOfInt[i-1], arrayOfInt[i+1]);
		return tempArray;
	}
}
