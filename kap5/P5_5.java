/**
 * 
 */
package kap5;

/**
 * Create a class that stores a string and that can manipulate it
 * in different ways with methods.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-27
 */
public class P5_5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		ManipulateString ms = new ManipulateString("Hejsan");
		System.out.println(ms.scramble());
		System.out.println(ms.scramble());
		System.out.println(ms.scramble());
		System.out.println(ms.scramble());
		System.out.println(ms.scramble());
		
		System.out.println(ms.getString());
		ms.insertAt(3, "san hej");
		System.out.println(ms.getString());
		ms.removeCharAt(6);
		System.out.println(ms.getString());
		
	}
}
