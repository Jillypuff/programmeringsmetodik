package kap5;

/**
 * Class storing array of int with methods to return different 
 * values based on it.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-26
 */

public class PrintArray {
	int[] arrayOfInt = new int[10];
	//int[] arrayOfInt = {3,6,4,-22,3,-66};
	
	public PrintArray() {

	}
	
	public void printArray() {
		for(int i : arrayOfInt)
			System.out.print(i + " ");
		System.out.println("");
	}
	public void printEvenIndex() {
		int temp = 0;
		for(int i : arrayOfInt) {
			if(temp % 2 == 0)
				System.out.print(i + " ");
			temp++;
		}
		System.out.println("");
	}
	public void printEvenElement() {
		for(int i : arrayOfInt) {
			if((i & 1) != 1)
				System.out.print(i + " ");
		}
		System.out.println("");
	}
	public void printReverse() {
		for(int i = arrayOfInt.length - 1; i >= 0; i--)
			System.out.print(arrayOfInt[i] + " ");
		System.out.println("");
	}
	public void printFirstAndLast() {
		System.out.println(arrayOfInt[0] + " " + arrayOfInt[arrayOfInt.length - 1]);
	}
	public int[] getArray() {
		return arrayOfInt.clone();
	}
}
