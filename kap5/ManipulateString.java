package kap5;

import java.util.Random;

/**
 * Class that gets and stores a string upon construction, then use
 * multiple different methods to send back different manipulated
 * versions of it.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-27
 */

public class ManipulateString {
	String str = "";
	
	public ManipulateString(String str) {
		this.str = str;
	}
	
	public String getString() {
		return str;
	}
	/**
	 * Scrambles the string
	 * @return String
	 */
	public String scramble() {
		if(str.length() < 3)
			return str;
		String tempString = str.substring(0,1);
		Random random = new Random();
		char[] charList = new char[str.length() - 2];
		for(int i = 0; i < str.length() - 2; i++)
			charList[i] = str.charAt(i+1);
		for(int i = 1; i < str.length() - 1; i++) {
			boolean valueFound = false;
			while(!valueFound) {
				int temp = random.nextInt(str.length() - 2);
				if(charList[temp] != '\u0000') {
					tempString += charList[temp];
					charList[temp] = '\u0000';
					valueFound = true;
				}
			}
		}
		
		return tempString + str.substring(str.length()-1, str.length());
	}
	public String reverse() {
		String tempString = "";
		for(int i = str.length() - 1; i >= 0; i--) {
			tempString += str.charAt(i);
		}
		return tempString;
	}
	public void insertAt(int index, String insertStr) {
		str = str.substring(0, index) + insertStr + str.substring(index, str.length());
	}
	public char removeCharAt(int index) {
		char returnChar = str.charAt(index);
		str = str.substring(0, index) + str.substring(index+1, str.length());
		return returnChar;
	}
}
