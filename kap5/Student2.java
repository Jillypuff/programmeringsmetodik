package kap5;

/**
 * Class with a student and all of their quizzes stored in an array.
 * Can fetch student name, total score, average score or specific quiz.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-26
 */

public class Student2 {
	String name;
	private static final int MAX_QUIZZES = 10;
	double[] quizzes = new double[MAX_QUIZZES];
	int nbrOfQuiz = 0;
	
	Student2(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public void addQuizScore(double newScore) {
		if(nbrOfQuiz < MAX_QUIZZES) {
			quizzes[nbrOfQuiz] = newScore;
			nbrOfQuiz++;
		}
	}
	public double getTotalScore() {
		double sum = 0.0;
		for(double d : quizzes)
			sum += d;
		return sum;
	}
	public double getAverageScore() {
		return nbrOfQuiz == 0 ? 0 : getTotalScore()/nbrOfQuiz;
	}
	public int getNbrOfQuiz() {
		return nbrOfQuiz;
	}
	public double getQuizScore(int index) {
		return quizzes[index];
	}
	public double[] getQuizzesCopy() {
		return quizzes.clone();
	}
	public String toString() {
		String returnString = "";
		for(int i = 0; i < nbrOfQuiz; i++) {
			returnString += i+1 + ": " + quizzes[i] + "\n";
		}
		return returnString;
	}
}