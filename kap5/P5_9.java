/**
 * 
 */
package kap5;

/**
 * Yatzy without the game and you can only roll dices.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-27
 */
public class P5_9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Yatzy y = new Yatzy(10);
		y.roll();
		System.out.println(y.toString());
		
	}

}
