package kap5;

/**
 * Creates a new matrix filled with 0. 
 * setValue will add a number if the place is empty 
 * and the number entered is not already in.
 * getValue will return the int currently on the row and columned entered.
 * isValidSquare will be true when all numbers have been filled in
 * toString will return a neat little string representing the board
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-27
 */

public class SudokuSquare {
	int[][] gameBoard;
	
	public SudokuSquare() {
		gameBoard = new int[3][3];
	}
	
	public void setValue(int value, int row, int col) {
		if(gameBoard[row][col] != 0) return;
		for(int[] i : gameBoard) {
			for(int j : i) {
				if(j == value)
					return;
			}
		}
		gameBoard[row][col] = value;
	}
	public int getValue(int row, int col) {
		return gameBoard[row][col];
	}
	public boolean isValidSquare() {
		for(int[] i : gameBoard) {
			for(int j : i) {
				if(j == 0)
					return false;
			}
		}
		return true;
	}
	public String toString() {
		String returnString = "";
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				returnString += gameBoard[i][j] + " ";
			}
			returnString += "\n";
		}
		return returnString;
	}
}
