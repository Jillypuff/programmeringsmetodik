package kap5;

import java.util.Arrays;

/**
 * Single line version of battleship. Have methods to add ships
 * of different sizes and see if an attack is a hit or miss.
 * Can print out the board to see currently how it looks.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-27
 */

public class PanamaBattleship {

	char[] panamaCanal;
	private static final char SHIP = 'X';
	private static final char WATER = '_';
	private static final char BOMBED_WATER = '*';
	private static final char BOMBED_SHIP = 'o';
	
	public PanamaBattleship(int canalLength) {
		panamaCanal = new char[canalLength];
		Arrays.fill(panamaCanal, WATER);
	}
	
	public void set2ShipAt(int index) {
		if(index < panamaCanal.length - 1) {
			panamaCanal[index] = SHIP;
			panamaCanal[index+1] = SHIP;
		} else
			System.out.println("Can't put ship there.");
	}
	public void set3ShipAt(int index) {
		if(index < panamaCanal.length - 2) {
			panamaCanal[index] = SHIP;
			panamaCanal[index+1] = SHIP;
			panamaCanal[index+2] = SHIP;
		} else
			System.out.println("Can't put ship there.");
	}
	public boolean attackPosition(int index) {
		if(index >= panamaCanal.length || index < 0) return false;
		if(panamaCanal[index] == BOMBED_SHIP || panamaCanal[index] == BOMBED_WATER)
			return false;
		panamaCanal[index] = (panamaCanal[index] == SHIP ? BOMBED_SHIP : BOMBED_WATER);
		return panamaCanal[index] == BOMBED_SHIP ? true : false;
	}
	public boolean isCanalClear() {
		for(char c : panamaCanal) {
			if(c == SHIP)
				return false;
		}
		return true;
	}
	public String toString() {
		String returnString = "";
		for(char c : panamaCanal)
			returnString += c;
		return returnString;
	}
}
