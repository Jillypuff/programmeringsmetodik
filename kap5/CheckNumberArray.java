package kap5;

/**
 * Class with array with number and methods that return
 * different values based on the array.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-26
 */

public class CheckNumberArray {
	private static final int MAX_ELEMENTS = 10;
	int[] arrayOfInt;
	int nbrOfElements = 0;
	
	public CheckNumberArray() {
		arrayOfInt = new int[MAX_ELEMENTS];
	}
	
	public void addNbr(int i) {
		if(nbrOfElements >= MAX_ELEMENTS) return;
		arrayOfInt[nbrOfElements] = i;
		nbrOfElements++;
	}
	public int getNbrOfElements() {
		return nbrOfElements;
	}
	public int getSum() {
		int sum = 0;
		for(int i : arrayOfInt) {
			sum += i;
		}
		return sum;
	}
	public double getAverage() {
		double sum = 0;
		for(int i : arrayOfInt) {
			sum += i;
		}
		return sum/nbrOfElements;
	}
	public boolean isAllTheSame() {
		int previousNbr = arrayOfInt[0];
		for(int i = 0; i < nbrOfElements; i++) {
			if(previousNbr != arrayOfInt[i])
				return false;
			else
				previousNbr = arrayOfInt[i];
		}
		return true;
	}
	public boolean isAccOrder() {
		int previousNbr = arrayOfInt[0];
		for(int i = 0; i < nbrOfElements; i++) {
			if(arrayOfInt[i] >= previousNbr)
				previousNbr = arrayOfInt[i];
			else
				return false;
		}
		return true;
	}
	public boolean isDesOrder() {
		int previousNbr = arrayOfInt[0];
		for(int i = 0; i < nbrOfElements; i++) {
			if(arrayOfInt[i] <= previousNbr)
				previousNbr = arrayOfInt[i];
			else
				return false;
		}
		return true;
		
	}
	public boolean isOrdered() {
		return isAccOrder() || isDesOrder();
	}
}
