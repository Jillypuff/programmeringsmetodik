package kap5;

import java.util.Random;

public class Dice {
	private int value;
	private Random rand = new Random();
	
	public Dice() {}
	
	public void roll() {
		value = rand.nextInt(6) + 1;
	}
	public int getValue() {
		return value;
	}
	public String toString() {
		return "[" + String.valueOf(value) + "]";
	}
}
