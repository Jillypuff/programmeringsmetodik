/**
 * 
 */
package kap5;

/**
 * Store a string in a class with methods that return different
 * information/versions of the string.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-26
 */
public class P5_4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CheckString2 cs2 = new CheckString2("pineapple pizza");
		
		System.out.println(cs2.findString("pizza"));
		System.out.println(cs2.countWords());
		System.exit(0);
	}

}
