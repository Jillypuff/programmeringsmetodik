/**
 * 
 */
package kap5;

import java.util.Random;
import java.util.Scanner;

/**
 * Single line version of battleship.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-27
 */
public class P5_8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int canalSize = 40;
		PanamaBattleship pb = new PanamaBattleship(canalSize);
		pb.set2ShipAt(3);
		pb.set3ShipAt(12);
		pb.set3ShipAt(28);
		pb.set2ShipAt(17);
		pb.set3ShipAt(55);
		
		
//		System.out.println(pb.toString());
//		Random rand = new Random();
//		while(!pb.isCanalClear()) {
//			pb.attackPosition(rand.nextInt(canalSize));
//			System.out.println(pb.toString());
//		}
		
//		Scanner attack = new Scanner(System.in);
//		while(!pb.isCanalClear()) {
//			System.out.print("Which index do you want to attack? ");
//			pb.attackPosition(attack.nextInt());
//			System.out.println(pb.toString());
//		}
//		attack.close();
			
		System.exit(0);
	}
}
