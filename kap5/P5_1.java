/**
 * 
 */
package kap5;

/**
 * Print different values of array
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-26
 */
public class P5_1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PrintArray pa = new PrintArray();
		pa.printArray();
		pa.printEvenElement();
		pa.printEvenIndex();
		pa.printReverse();
		pa.printFirstAndLast();
		
	}

}
