package kapitel1;

public class P1_2 {
	public static void main(String[] args) {
		int heltal = 1;
		int total_summa = 0;
		while (heltal <= 10){
			total_summa += heltal;
			heltal++;
			System.out.println("Totala summan är just nu: " + total_summa + ".");
		}
		System.out.println("Totala summan blev: " + total_summa + ".");
	}
}
