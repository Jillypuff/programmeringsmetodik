package kapitel1;

public class P1_4 {
	public static void main(String[] args) {
		double konto = 1000; // Pengar ifrån början
		double rent = 0.05;	// 5% ränta
		System.out.println("Pengar på kontot från början: " + konto + ".");
		for (int i = 0; i < 3; i++){
			konto += konto * rent; // Ökning på kontot efter ett år
			System.out.println("Pengar på konto efter " + i + " år är " + konto + ".");
		}
	}
}
