/**
 * 
 */
package kap9;

/**
 * Returns second largest element of a collections.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */
public class P9_5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		int[] arr1 = {4, 3, 2, 1, 2, 3, 4};
		int[] arr2 = {1, 3, 4, 5, 6, 7};
		int[] arr3 = {100, 90, 95, 90};
		
		System.out.println(getSecondMax(arr1));
		System.out.println(getSecondMax(arr2));
		System.out.println(getSecondMax(arr3));
		
	}
	
	
	public static int getSecondMax(int[] array) {
		if(array.length == 0)
			return 0;
		if(array.length == 1)
			return array[0];
		int max = array[0];
		int secondMax;
		if(array[1] > max) {
			secondMax = max;
			max = array[1];
		} else {
			secondMax = array[1];
		}
		
		for(int i = 2; i < array.length; i++) {
			if(array[i] > max) {
				secondMax = max;
				max = array[i];
			} else if (array[i] > secondMax && array[i] < max)
				secondMax = array[i];
		}
		return secondMax;
	}
	
//	public static int getSecondMax(int[] array) {
//		if(array.length == 0)
//			return 0;
//		if(array.length == 1)
//			return array[0];
//		int max = array[0];
//		int secondMax = array[0];
//		
//		if(array.length >= 2 && max < array[1])
//			secondMax = array[1];
//		for(int i = 2; i < array.length; i++) {
//			if(array[i] > max) {
//				secondMax = max;
//				max = array[i];
//			}
//		}
//		return secondMax;
//	}
}
