/**
 * 
 */
package kap9;

/**
 * Test for our student class.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */
public class P9_1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		StudentWithArrayList s = new StudentWithArrayList("Steve");
		s.addQuizScore(5.5);
		s.addQuizScore(100);
		System.out.println(s);
		
		System.out.println(s.getQuizzes());
		try {
			s.addQuizScore(-55);
		} catch (Exception e) {
			System.err.println("Exception: " + e.getLocalizedMessage());
			System.exit(1);
		}
		System.exit(0);		
	}

}
