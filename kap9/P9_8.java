/**
 * 
 */
package kap9;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Checks if two strings are permutations of eachother.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */
public class P9_8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String wordOne = "hejj";
		String wordTwo = "jjeh";
		System.out.println(isPermutation(wordOne, wordTwo));
		
	}
	
	public static boolean isPermutation(String wordOne, String wordTwo) {
		if(wordOne == null || wordTwo == null)
			throw new IllegalArgumentException("Minst en parameter var null.");
		if(wordOne.isBlank() || wordTwo.isBlank())
			throw new IllegalArgumentException("Minst en parameter var tom.");
		if(wordOne.length() != wordTwo.length())
			return false;
		if(wordOne.equals(wordTwo))
			return false;
		
		ArrayList<Character> wordOneList = createArrayList(wordOne);
		ArrayList<Character> wordTwoList = createArrayList(wordTwo);
		Collections.sort(wordOneList);
		Collections.sort(wordTwoList);
		for(int i = 0; i < wordOneList.size(); i++)
			if(Character.compare(wordOneList.get(i), wordTwoList.get(i)) != 0)
				return false;
		return true;
	}
	
	public static ArrayList<Character> createArrayList(String word){
		ArrayList<Character> temp = new ArrayList<>();
		for(char c : word.toCharArray())
			temp.add(c);
		return temp;	
	}

}
