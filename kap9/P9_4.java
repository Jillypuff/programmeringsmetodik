/**
 * 
 */
package kap9;

/**
 * Check if every numbers in array varies between strictly
 * ascending and strictly descending.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */
public class P9_4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] arr1 = {1, 3, 2, 5, 4, 19, 3, 4, 1};
		int[] arr2 = {1, 3, 4, 5, 6, 7};
		int[] arr3 = {45, 45, 45, 46, 23, 34};
		
		System.out.println(checkStrictAscDecValues(arr1));
		System.out.println(checkStrictAscDecValues(arr2));
		System.out.println(checkStrictAscDecValues(arr3));

		
	}
	
	public static boolean checkStrictAscDecValues(int[] array) {
		if(array.length > 1) {
			for(int i = 1; i < array.length; i+=2) {
				if(array[i] <= array[i-1])
					return false;
				if(i + 1 != array.length)
					if(array[i] <= array[i+1])
						return false;
			}
		}
		return true;
	}
	
//	public static boolean checkStrictAscDecValues(int[] array) {
//		if(array.length > 1) {
//			for(int i = 1; i < array.length; i++) {
//				if(i%2 == 0 && array[i] >= array[i-1])
//					return false;
//				if(i%2 == 1 && array[i] <= array[i-1])
//					return false;
//			}
//		}
//		return true;
//	}
}
