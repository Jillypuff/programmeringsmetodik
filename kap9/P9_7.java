/**
 * 
 */
package kap9;

/**
 * Reverses every word in a sentence.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */
public class P9_7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String sentence = "    Hej    du    där      borta Test hmm";
		System.out.println(reverseWordInSentence(sentence));
		
	}
	
	public static String reverseWordInSentence(String sentence) {
		if(sentence.length() < 2)
			return sentence;
		String returnString = "";
		String[] words = sentence.split("\\s", 0);
		for(String word : words)
			returnString += reverseWord(word) + " ";
		if(returnString.length() > 2)
			returnString = returnString.substring(0, returnString.length() - 1);
		return returnString;
	}
	
	public static String reverseWord(String word) {
		String returnString = "";
		for(int i = word.length() - 1; i >= 0 ; i--) {
			returnString += word.charAt(i);
		}
		return returnString;
	}

}
