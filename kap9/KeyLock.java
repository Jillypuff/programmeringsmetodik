/**
 * 
 */
package kap9;

/**
 * Child class of Lock, add a key feature in form of a string.
 * Need to enter correct key string to be able to open.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */
public class KeyLock extends Lock{
	
	private String key;
	
	public KeyLock(String key){
		if(key == null)
			throw new IllegalArgumentException("Cannot create a lock with key null.");
		this.key = key;
	}
	
	public void open() {
		this.open("");
	}
	
	public void open(String input) {
		if(input == null)
			throw new IllegalArgumentException("Cannot try to open key with null.");
		if(key.equals(input))
			super.open();
	}
	
	public void changeKey(String newKey) {
		if(newKey == null)
			throw new IllegalArgumentException("Cannot set key to null.");
		if(super.isOpen())
			key = newKey;
	}
	
}
