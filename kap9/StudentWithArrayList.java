/**
 * 
 */
package kap9;

import java.util.ArrayList;

/**
 * Storing quizzez for a student.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */
public class StudentWithArrayList {

	private String name;
	private ArrayList<Double> quizzes = new ArrayList<>();

	public StudentWithArrayList(String name) {
		if(name.isBlank())
			throw new IllegalArgumentException("Name is empty.");
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void addQuizScore(double score) {
		if (score < 0)
			throw new IllegalArgumentException("Cannot add a negative score.");
		quizzes.add(score);
	}

	public double getTotalScore() {
		return quizzes.stream().mapToDouble(Double::doubleValue).sum();
	}

	public double getAverageScore() {
		return getTotalScore() / quizzes.size();
	}

	public int getNbrOfQuiz() {
		return quizzes.size();
	}

	public double getQuizScore(int index) {
		if (index >= quizzes.size())
			throw new IndexOutOfBoundsException("Index is out of bounds.");
		return quizzes.get(index);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Double> getQuizzes() {
		return (ArrayList<Double>) quizzes.clone();
	}

	public String toString() {
		String returnString = "";
		for (double d : quizzes)
			returnString += d + "\n";
		return returnString.isBlank() ? "No quizzes added yet." : returnString;
	}
}
