/**
 * 
 */
package kap9;

/**
 * Test class for Lock and KeyLock.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */
public class P9_10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Lock lk = new Lock();
		System.out.println(lk.isOpen());
		lk.close();
		System.out.println(lk.isOpen());
		
		KeyLock kl = new KeyLock("Horse");
		System.out.println(kl.isOpen());
		kl.close();
		System.out.println(kl.isOpen());
		kl.open();
		System.out.println(kl.isOpen());
		kl.open("Horse");
		System.out.println(kl.isOpen());
		kl.close();
		kl.open("Dog");
		System.out.println(kl.isOpen());
		kl.open("Horse");
		kl.changeKey("Monkey");
		kl.close();
		System.out.println(kl.isOpen());
		kl.open("Monkey");
		System.out.println(kl.isOpen());
	}

}
