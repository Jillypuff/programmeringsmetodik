/**
 * 
 */
package kap9;

import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Tic Tac Toe.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */

public class P9_9 {
	
	public enum Status {
		
		NO_WIN,
		X_WIN,
		O_WIN,
		REMI;
		
	}
	
	
	/**
	 * @param args
	 */
	
	public static void main(String[] args) {
		char[][] gameBoard = {{'-', '-', '-'},{'-', '-', '-'},{'-', '-', '-'}};
		gameLoop(gameBoard);
		System.exit(0);
	}
	
	public static void gameLoop(char[][] gameBoard) {
		printBoard(gameBoard);
		int spelare = 1;
		Scanner input = new Scanner(System.in);
		Status gameStatus = Status.NO_WIN;
		while(gameStatus == Status.NO_WIN) {
			char[] playerInput = getMove(gameBoard, input);
			gameBoard = addMove(playerInput, gameBoard);
			gameStatus = gameCheck(gameBoard);
			spelare = spelare % 2 + 1;
		}
		switch(gameStatus) {
		case X_WIN -> System.out.println("Spelare 1 vann");
		case O_WIN -> System.out.println("Spelare 2 vann");
		case REMI -> System.out.println("Oavgjort");
		default -> System.out.println("Something went wrong.");
		}
		input.close();
	}
	
	public static char[] getMove(char[][] gameBoard, Scanner input) {
		String correctMoveRegex = "([1-3]) ([1-3]) (x|o|X|O)";
		char[] playerInput = new char[3];
		boolean correctInput = false;
		while(!correctInput) {
			System.out.print("Please enter your move: ");
			String userMove = input.nextLine();
			if(Pattern.matches(correctMoveRegex, userMove)) {
				playerInput[0] = userMove.charAt(0);
				playerInput[1] = userMove.charAt(2);
				playerInput[2] = userMove.charAt(4);
				if(availableSlot(playerInput, gameBoard))
					correctInput = true;
			} else
				System.out.println("Incorrect input.");
		}
		return playerInput;
	}
	
	public static boolean availableSlot(char[] playerInput, char[][] gameBoard) {
		int[] rowCol = charToRowCol(playerInput);
		if(gameBoard[rowCol[0]][rowCol[1]] == '-')
			return true;
		System.out.println("There already is a " + gameBoard[rowCol[0]][rowCol[1]] + " on that spot.");
		return false;
	}
	
	public static char[][] addMove(char[] playerInput, char[][] gameBoard){
		int[] rowCol = charToRowCol(playerInput);			
		gameBoard[rowCol[0]][rowCol[1]] = playerInput[2] == 'x' || playerInput[2] == 'X' ? 'x' : 'o';
		printBoard(gameBoard); 
		return gameBoard;
	}
	
	public static int[] charToRowCol(char[] playerInput) {
		int[] rowCol = new int[2];
		if(playerInput[0] == '1')
			rowCol[0] = 0;
		else
			rowCol[0] = playerInput[0] == '2' ? 1 : 2;
		if(playerInput[1] == '1')
			rowCol[1] = 0;
		else
			rowCol[1] = playerInput[1] == '2' ? 1 : 2;
		return rowCol;
	}
	
	public static void printBoard(char[][] gameBoard) {
		System.out.println( gameBoard[0][0] + " " + gameBoard[0][1] + " " + gameBoard[0][2] + "\n" +
							gameBoard[1][0] + " " + gameBoard[1][1] + " " + gameBoard[1][2] + "\n" +
							gameBoard[2][0] + " " + gameBoard[2][1] + " " + gameBoard[2][2]);
	}
	
	public static Status gameCheck(char[][] gameBoard) {
		int x = 0;
		int o = 0;
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				if(gameBoard[i][j] == '-')
					break;
				if(gameBoard[i][j] == 'x')
					x++;
				if(gameBoard[i][j] == 'o')
					o++;	
			}
			if(x == 3)
				return Status.X_WIN;
			if(o == 3)
				return Status.O_WIN;
			x = 0;
			o = 0;
		}
		
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				if(gameBoard[j][i] == '-')
					break;
				if(gameBoard[j][i] == 'x')
					x++;
				if(gameBoard[j][i] == 'o')
					o++;	
			}
			if(x == 3)
				return Status.X_WIN;
			if(o == 3)
				return Status.O_WIN;
			x = 0;
			o = 0;
		}
		
		if(gameBoard[1][1] == '-')
			return Status.NO_WIN;
		if(gameBoard[1][1] == 'x')
			if(gameBoard[0][0] == 'x' && gameBoard[2][2] == 'x' || gameBoard[0][2] == 'x' && gameBoard[2][0] == 'x')
				return Status.X_WIN;
		if(gameBoard[1][1] == 'o')
			if(gameBoard[0][0] == 'o' && gameBoard[2][2] == 'o' || gameBoard[0][2] == 'o' && gameBoard[2][0] == 'o')
				return Status.O_WIN;
		
		for(char[] ch : gameBoard)
			for(char c : ch)
				if(c == '-')
					return Status.NO_WIN;
		return Status.REMI;
	}
}
