/**
 * 
 */
package kap9;

/**
 * Test for our work week class.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */
public class P9_2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		GeneralWorkweek gw = new GeneralWorkweek();
		gw.addWeekday(Weekday.MONDAY);
		gw.addWeekday(Weekday.TUESDAY);
		gw.addWeekday(Weekday.WEDNESDAY);
		gw.addWeekday(Weekday.THURSDAY);
		gw.addWeekday(Weekday.FRIDAY);
		gw.addWeekday(Weekday.SATURDAY);
		
		System.out.println(gw.getWorktime());
		
	}

}
