/**
 * 
 */
package kap9;

/**
 * Class to store total worktime for a week.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */
public class ProgrammingWorkweek {
	
	private double totalWorkTime;
	
	public ProgrammingWorkweek() {}
	
	public void addWeekday(ProgrammingWeekday day) {
		totalWorkTime += day.getTime();
	}

	public double getWorktime() {
		return totalWorkTime;
	}
}
