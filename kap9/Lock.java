/**
 * 
 */
package kap9;

/**
 * Parent Lock class with only the basics functions of a lock.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */
public class Lock {
	
	private boolean isOpen;
	
	public Lock() {
		isOpen = true;
	}
	
	public void open() {
		isOpen = true;
	}
	
	public void close() {
		isOpen = false;
	}
	
	public boolean isOpen() {
		return isOpen;
	}
}
