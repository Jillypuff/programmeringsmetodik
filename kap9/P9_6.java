/**
 * 
 */
package kap9;

import java.util.Arrays;

/**
 * Returns the median of every other value in a collections.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */
public class P9_6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		int[] arr1 = {1, 10, 5, 7, 6, 12, 3, 8, 2};
		int[] arr2 = {3, 3, 8, 12, 8, 11, 2, 9};
		int[] arr3 = {100, 90, 95, 90};
		
		int[] arr4 = oddMedianElements(arr1);
		int[] arr5 = oddMedianElements(arr2);
		int[] arr6 = oddMedianElements(arr3);
		
		for(int i : arr4)
			System.out.println(i);
		for(int i : arr5)
			System.out.println(i);
		for(int i : arr6)
			System.out.println(i);
		
	}
	
	public static int[] oddMedianElements(int[] array) {
		int[] tempArray = new int[array.length/2 + array.length % 2];
		for(int i = 0; i < array.length; i += 2) {
			tempArray[i/2] = array[i];
		}
		Arrays.sort(tempArray);
		int length = tempArray.length;
		if(length % 2 == 1)
			return new int[] {tempArray[length / 2]};
		return new int[] {tempArray[length / 2 - 1], tempArray[length / 2]};
	}
}
