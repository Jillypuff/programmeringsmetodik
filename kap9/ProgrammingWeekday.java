/**
 * 
 */
package kap9;

/**
 * Enumerator to keep track of the days and worktime for said day.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */
public enum ProgrammingWeekday {
	
	MONDAY(8.0),
	TUESDAY(2.0),
	WEDNESDAY(4.5),
	THURSDAY(2.0),
	FRIDAY(6.0),
	SATURDAY(0.0),
	SUNDAY(0.0);
	
	double time;

	ProgrammingWeekday(double time) {
		this.time = time;
	}

	public double getTime() {
		return time;
	}
}
