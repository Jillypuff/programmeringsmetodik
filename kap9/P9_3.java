/**
 * 
 */
package kap9;

/**
 * Test class for enumerate ProgrammingWeekday and the 
 * ProgrammingWorkweek class.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */
public class P9_3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		ProgrammingWorkweek pw = new ProgrammingWorkweek();
		pw.addWeekday(ProgrammingWeekday.MONDAY);
		pw.addWeekday(ProgrammingWeekday.TUESDAY);
		pw.addWeekday(ProgrammingWeekday.THURSDAY);
		System.out.println(pw.getWorktime());
		
	}
}
