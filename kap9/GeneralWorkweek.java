/**
 * 
 */
package kap9;

/**
 * Class to add and return total work time for a week.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-24
 *
 */
public class GeneralWorkweek {
	
	private double totalWorkTime;
	
	public GeneralWorkweek() {}
	
	public double getWorktime() {
		return totalWorkTime;
	}

	public void addWeekday(Weekday day) {
		switch (day) {
		case MONDAY -> totalWorkTime += 8;
		case TUESDAY -> totalWorkTime += 8;
		case WEDNESDAY -> totalWorkTime += 8;
		case THURSDAY -> totalWorkTime += 8;
		case FRIDAY -> totalWorkTime += 6;
		case SATURDAY, SUNDAY -> totalWorkTime += 0;
		default -> System.out.println("Not a day.");
		}
	}
}
