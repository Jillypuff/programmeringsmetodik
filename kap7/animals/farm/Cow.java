/**
 * 
 */
package kap7.animals.farm;

import kap7.animals.Animal;

/**
 * Subclass to Animal for cow specifics.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class Cow extends Animal{

	private int id;
	
	public Cow(int id, int birthYear) {
		super("Inget namn", birthYear);
		this.id = id;
	}
	
	public String makeSound() {
		return "Mu mu!";
	}
	
	public String getDescription() {
		return "Cow id: " + id + ". Cow age: " + super.getAge() + ".";
	}
	
}
