/**
 * 
 */
package kap7.animals.farm;

import kap7.animals.Animal;

/**
 * Subclass to Animal for pig specifics.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class Pig extends Animal{

	private int id;
	
	public Pig(int id, int birthYear) {
		super("Inget namn", birthYear);
		this.id = id;
	}
	
	public String makeSound() {
		return "Nöff nöff!";
	}
	
	public String getDescription() {
		return "Pig id: " + id + ". Pig age: " + super.getAge() + ".";
	}
	
}
