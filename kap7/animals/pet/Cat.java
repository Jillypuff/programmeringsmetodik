/**
 * 
 */
package kap7.animals.pet;

import kap7.animals.Animal;

/**
 * Subclass to Animal for cat specifics.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class Cat extends Animal{
	
	private boolean indoor;
	
	public Cat(String name) {
		super(name);
		indoor = false;
	}
	
	public Cat(String name, int birthYear) {
		super(name, birthYear);
		indoor = false;
	}
	
	public Cat(String name, int birthYear, boolean indoor) {
		super(name, birthYear);
		this.indoor = indoor;
	}
	
	public String makeSound() {
		return "Mjau, mjau";
	}
	
	public String getDescription() {
		return "Cat name: " + super.getName() + ". Cat age: " + super.getAge() + ". Indoor: " + isIndoor() + ".";
	}
	
	public boolean isIndoor() {
		return indoor;
	}
}
