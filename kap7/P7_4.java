/**
 * 
 */
package kap7;

import kap7.animals.pet.Cat;
import kap7.animals.pet.Dog;

/**
 * Test the subclasses Cat and Dog.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */

public class P7_4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Cat cat = new Cat("Steve");
		System.out.println(cat.getDescription());
		
		Dog dog = new Dog("George", 1992, true);
		System.out.println(dog.getDescription());
		
	}

}
