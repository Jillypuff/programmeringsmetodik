/**
 * 
 */
package kap7;

/**
 * Subclass to Question for MultiChoiceQuestions.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class MultiChoiceQuestion extends Question{
	
	String[] choices = new String[10];
	int[] correctChoicesNbr = new int[10];
	
	public void addChoice(String answer, boolean correct) {
		int answerNumber = 0;
		for(int i = 0; i < 10; i++)
			if(choices[i] == null) {
				choices[i] = answer;
				answerNumber = i+1;
				break;
			}
		if(correct)
			for(int i = 0; i < 10; i++)
				if(correctChoicesNbr[i] == 0) {
					correctChoicesNbr[i] = answerNumber;
					break;
				}
	}
	
	public boolean checkAnswer(String answers) {
		boolean correctAnswer = true;
		for(int i = 0; i < 10; i++) {
			if(correctChoicesNbr[i] == 0)
				break;
			if(!answers.contains(String.valueOf(correctChoicesNbr[i]))) {
				correctAnswer = false;
				break;
			}
		}
		return correctAnswer;
	}
	
	public String toString() {
		String returnString = super.toString() + "\n";
		for(int i = 0; i < 10; i++) {
			if(choices[i] == null)
				break;
			returnString += (i + 1) + " - " + choices[i] + "\n";
		}
		return returnString;
	}
}
