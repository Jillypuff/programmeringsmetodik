package kap7;

/**
 * Tally counter with limit
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */

public class P7_2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		TallyCounterLimit tcl = new TallyCounterLimit();
		for(int i = 0; i < 5000; i++)
			tcl.count();
		System.out.println(tcl.getValue());
		tcl.setLimit(5000);
		try {
			tcl.count();
		} catch (Exception e) {
			System.err.println("Exception: " + e.getLocalizedMessage());
			System.exit(1);
		}
		
	}

}
