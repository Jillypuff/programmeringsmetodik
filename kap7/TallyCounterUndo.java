package kap7;

/**
 * Subclass to TallyCounter to add undo functionality.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 * 
 */

public class TallyCounterUndo extends TallyCounter{
	
	public TallyCounterUndo() {
		
	}
	
	public void undo() {
		if(super.counter < 1)
			super.reset();
		super.counter--;
	}
	
}
