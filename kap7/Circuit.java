/**
 * 
 */
package kap7;

/**
 * Abstract blueprint for a circuit.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
abstract class Circuit {
	
	abstract double getResistance();
	
}
