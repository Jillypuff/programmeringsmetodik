/**
 * 
 */
package kap7;

/**
 * Testing different circuits works correctly.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class P7_10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Resistor r1 = new Resistor(10);
		Resistor r2 = new Resistor(100);
		
		SerialCircuit sc = new SerialCircuit(r1);
		ParallelCircuit pc = new ParallelCircuit(r2);
		sc.addCircuit(r1);
		pc.addCircuit(sc);
		System.out.println(pc.getResistance());
		System.out.println(sc.getResistance());
		pc.addCircuit(sc);
		pc.addCircuit(sc);
		System.out.println(pc.getResistance());
		
	}
}
