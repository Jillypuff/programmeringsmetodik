/**
 * 
 */
package kap7;

import kap7.animals.farm.Cow;
import kap7.animals.farm.Pig;
import kap7.animals.pet.Cat;
import kap7.animals.pet.Dog;

/**
 * Test class for all our animals.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class P7_5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Cat cat = new Cat("George", 1999, true);
		Dog dog = new Dog("Steve");
		Pig pig = new Pig(5555, 2012);
		Cow cow = new Cow(1234, 2020);
		
		System.out.println(cat.getDescription());
		System.out.println(dog.getDescription());
		System.out.println(pig.getDescription());
		System.out.println(cow.getDescription());
		
	}

}
