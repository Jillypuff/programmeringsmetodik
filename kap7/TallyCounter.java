package kap7;

/**
 * Create a TallyCounter to count to limit or reset.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */

public class TallyCounter {

	protected int counter = 0;

	public TallyCounter() {
	}

	public void count() {
		if (this.counter < Integer.MAX_VALUE)
			this.counter++;
	}

	public int getValue() {
		return this.counter;
	}

	public void reset() {
		this.counter = 0;
	}
}