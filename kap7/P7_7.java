/**
 * 
 */
package kap7;

/**
 * Test the number version of questions.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class P7_7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		NumericQuestion nq = new NumericQuestion();
		nq.setText("What is the first 2 decimals of pi?");
		nq.setAnswer("3,14");
		
		String testAnswer = "3,16";
		System.out.println(nq.toString());
		System.out.println("Is " + testAnswer + " correct? " + nq.checkAnswer(testAnswer));
		testAnswer = "3,14";
		System.out.println("Is " + testAnswer + " correct? " + nq.checkAnswer(testAnswer));
		testAnswer = "3,15";
		System.out.println("Is " + testAnswer + " correct? " + nq.checkAnswer(testAnswer));
		testAnswer = "3,13";
		System.out.println("Is " + testAnswer + " correct? " + nq.checkAnswer(testAnswer));

		
	}

}
