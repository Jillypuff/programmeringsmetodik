/**
 * 
 */
package kap7;

import java.util.ArrayList;

/**
 * Class storing the Serial Circuits
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class SerialCircuit extends Circuit{
	private ArrayList<Circuit> circuit = new ArrayList<>();
	
	public SerialCircuit(Circuit circuit) {
		addCircuit(circuit);
	}
	
	public double getResistance() {
		double resistance = 0.0;
		for(Circuit cir : circuit)
			resistance += cir.getResistance();
		return resistance;
	}
	
	public void addCircuit(Circuit circuit) {
		this.circuit.add(circuit);
	}
}
