/**
 * 
 */
package kap7;

/**
 * Test the Pet class.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */

public class P7_3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Pet pet = new Pet(null);
		
		System.out.println(pet.getAge());
		System.out.println(pet.getName());
		
		pet.setName("George");
		System.out.println(pet.getName());
		
	}

}
