/**
 * 
 */
package kap7;

/**
 * Stores a questions and answer to said questions then check if
 * the correct answer is beeing given.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class Question {

	private String question;
	private String answer;
	
	public Question() {
		
	}
	
	public void setText(String question) {
		this.question = question;
	}
	
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	public String getAnswer() {
		return answer;
	}
	
	public boolean checkAnswer(String response) {
		return answer.replaceAll("-", "").replaceAll(" +", " ").trim().equalsIgnoreCase(response.replaceAll("-", "").replaceAll(" +", " ").trim());
	}
	
	public String toString() {
		return question;
	}
}
