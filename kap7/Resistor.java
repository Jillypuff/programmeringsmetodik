/**
 * 
 */
package kap7;

/**
 * Create a resistor of any resistance to add to any circuits.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class Resistor extends Circuit{
	
	private double resistance;
	
	public Resistor(double resistor){
		resistance = resistor;
	}
	
	public double getResistance() {
		return resistance;
	}
	
}
