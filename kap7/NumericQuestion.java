/**
 * 
 */
package kap7;

/**
 * Add a check for number questions
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class NumericQuestion extends Question{
	
	public boolean checkAnswer(String answer) {
		return (Math.abs(Double.parseDouble(answer.replaceAll(",", ".")) - Double.parseDouble(super.getAnswer().replaceAll(",", "."))) < 0.01);
	}
}
