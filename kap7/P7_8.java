/**
 * 
 */
package kap7;

/**
 * Testing the FillinQuestion version of Question class.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class P7_8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		FillInQuestion fq = new FillInQuestion();
		fq.setText("Test _quest_ here.");
		System.out.println(fq.getAnswer());
		System.out.println(fq);
		
	}

}