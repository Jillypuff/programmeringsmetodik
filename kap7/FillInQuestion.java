/**
 * 
 */
package kap7;

/**
 * Subclass to Question for Fill In Questions.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class FillInQuestion extends Question{
	
	public void setText(String fillInQuestion) {
		int startOfAnswer = fillInQuestion.indexOf("_");
		int endOfAnswer = fillInQuestion.lastIndexOf("_");
		String question = fillInQuestion.substring(0, startOfAnswer);
		for(int i = 0; i < endOfAnswer - startOfAnswer - 1; i ++) {
			question += "_";
		}
		question += fillInQuestion.substring(endOfAnswer + 1);
		
		super.setText(question);
		super.setAnswer(fillInQuestion.substring(startOfAnswer + 1, endOfAnswer));
	}
	
}
