package kap7;

/**
 * Subclass to TallyCounter to add a limit to the counter.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */

public class TallyCounterLimit extends TallyCounterUndo{
	
	private int limit;
	
	public TallyCounterLimit() {
		setLimit(Integer.MAX_VALUE);
	}
	
	public TallyCounterLimit(int limit) {
		setLimit(limit);
	}
	
	public void count() {
		if(super.counter >= limit)
			throw new IllegalStateException("Above counter limit.");
		super.counter++;
	}
	
	public void setLimit(int limit) {
		if(limit < 0)
			throw new IllegalArgumentException("Cannot have a negative limit.");
		this.limit = limit;
	}
}
