/**
 * 
 */
package kap7;

/**
 * Subclass to Animal for dog specifics.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class Dog extends Pet{
	
	private boolean hunting;
	
	public Dog(String name) {
		super(name);
		hunting = false;
	}
	
	public Dog(String name, int birthYear) {
		super(name, birthYear);
		hunting = false;
	}
	
	public Dog(String name, int birthYear, boolean hunting) {
		super(name, birthYear);
		this.hunting = hunting;
	}
	
	public String makeSound() {
		return "Vov vov!";
	}
	
	public String getDescription() {
		return "Dog name: " + super.getName() + ". Dog age: " + super.getAge() + ". Indoor: " + isHunting() + ".";
	}
	
	public boolean isHunting() {
		return hunting;
	}
}
