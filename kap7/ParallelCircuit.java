/**
 * 
 */
package kap7;

import java.util.ArrayList;

/**
 * Class storing the Parallel Circuits.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class ParallelCircuit extends Circuit{
	
	private ArrayList<Circuit> circuit = new ArrayList<>();
	
	public ParallelCircuit(Circuit circuit) {
		addCircuit(circuit);
	}
	
	public double getResistance() {
		double resistance = 0.0;
		for(Circuit cir : circuit)
			resistance += 1 / cir.getResistance();
		
		return 1 / resistance;
	}
	
	public void addCircuit(Circuit circuit) {
		this.circuit.add(circuit);
	}
}
