/**
 * 
 */
package kap7;

import java.util.Calendar;

/**
 * Class to create and track pets.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */

public class Pet {
	
	private String name;
	private int birthYear;
	
	public Pet(String name) {
		this(name, Calendar.getInstance().get(Calendar.YEAR));
	}
	
	public Pet(String name, int birthYear) {
		setName(name);
		setBirthYear(birthYear);
	}
	
	public void setName(String name) {
		if(name == null)
			throw new IllegalArgumentException("Enter a valid name.");
		if(name.length() == 0)
			throw new IllegalArgumentException("Enter a valid name.");
		this.name = name;
	}
	
	public void setBirthYear(int birthYear) {
		if(birthYear > Calendar.getInstance().get(Calendar.YEAR)) {
			throw new IllegalArgumentException("Enter a valid year.");
		}
		this.birthYear = birthYear;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getAge() {
		return Calendar.getInstance().get(Calendar.YEAR) - this.birthYear;
	}
	
}