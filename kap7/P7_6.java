/**
 * 
 */
package kap7;

/**
 * Check if questions class works.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class P7_6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Question q1 = new Question();
		
		q1.setText("What is the square root of 16?");
		q1.setAnswer("     4----    ");
		
		
		String testAnswer = "5";
		System.out.println("Is " + testAnswer + " correct? " + q1.checkAnswer(testAnswer));
		System.out.println(q1.getAnswer());
		testAnswer = " -4 --- -  ";
		System.out.println("Is " + testAnswer + " correct? " + q1.checkAnswer(testAnswer));

	}

}
