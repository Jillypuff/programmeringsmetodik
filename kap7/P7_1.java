/**
 * 
 */
package kap7;

/**
 * Test TallyCounterUndo
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-10
 *
 */
public class P7_1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		TallyCounterUndo tcu = new TallyCounterUndo();
		tcu.count();
		System.out.println(tcu.getValue());
		tcu.undo();
		System.out.println(tcu.getValue());
		tcu.undo();
		System.out.println(tcu.getValue());
	}

}
