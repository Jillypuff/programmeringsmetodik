package kap4;

public class SodaCan {
	private double height;
	private double radius;
	
	public SodaCan(double height, double radius) {
		this.height = height;
		this.radius = radius;
	}
	
	public double getHeight() {
		return height;
	}
	public double getRadius() {
		return radius;
	}
	public double getVolume() {
		return height * Math.PI * Math.pow(radius, 2);
	}
	public double getSurfaceArea() {
		return 2 * Math.PI * radius * height + 2 * Math.PI * Math.pow(radius, 2);
	}
	public double getBottomArea() {
		return Math.pow(radius, 2) * Math.PI;
	}
	public double getCircumference() {
		return 2 * Math.PI * radius;
	}
}
