/**
 * 
 */
package kap4;

/**
 * Takes height and radius of a soda can and return different facts.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-19
 */
public class P4_3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SodaCan sc = new SodaCan(5.5, 3.3);
		
		System.out.format("Soda can height: %.2f\nSoda can radius: %.2f\n", sc.getHeight(), sc.getRadius());
		System.out.format("The volume is: %.2f\n", sc.getVolume());
		System.out.format("The surface area is: %.2f\n", sc.getSurfaceArea());
		System.out.format("The bottom area is: %.2f\n", sc.getBottomArea());
		System.out.format("The circumference is: %.2f\n", sc.getCircumference());
		
		System.exit(0);
	}
}
