package kap4;

public class TallyCounter2 {
	private int counter = 0;
	private int limit = Integer.MAX_VALUE;
	
	public TallyCounter2() {}

	public void count() {
		if(counter < limit)
			counter++;
		else
			System.out.println("Limit reached.");
	}	
	public int getValue() {
		return counter;
	}
	public void reset() {
		counter = 0;
	}
	public void undo() {
		counter = Math.max(0, counter-1);
	}
	public void setLimit(int i) {
		limit = i;
	}
}