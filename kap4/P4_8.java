/**
 * 
 */
package kap4;

/**
 * Create a class Bug with a bug that can move back and forth.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-19
 */
public class P4_8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Bug bug = new Bug(5);
		System.out.printf("Krypets position är: %d%n", bug.getPosition());
		bug.move();
		bug.move();
		System.out.printf("Krypets position är: %d%n", bug.getPosition());
		bug.turn();
		bug.move();
		System.out.printf("Krypets position är: %d%n", bug.getPosition());
		bug.move();
		bug.turn();
		bug.turn();
		bug.move();
		System.out.printf("Krypets position är: %d%n", bug.getPosition());
		bug.turn();
		bug.turn();
		bug.turn();
		bug.move();
		System.out.printf("Krypets position är: %d%n", bug.getPosition());
				
		
		System.exit(0);
	}

}
