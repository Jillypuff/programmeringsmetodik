package kap4;

public class Student {
	private String name;
	private double totalScore;
	private int nbrOfQuiz;
	
	public Student(String name) {
		this.name = name;
	}
	
	public void addQuizScore(double score) {
		totalScore += score;
		nbrOfQuiz++;
	}
	public String getName() {
		return name;
	}
	public double getTotalScore() {
		return totalScore;
	}
	public double getAverageScore() {
		return totalScore/nbrOfQuiz;
	}
	public int getNbrOfQuiz() {
		return nbrOfQuiz;
	}
}
