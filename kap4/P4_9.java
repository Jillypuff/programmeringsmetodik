/**
 * 
 */
package kap4;

/**
 * Create a class Dice that can create dice that roll 1-6.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-19
 */
public class P4_9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Dice dice = new Dice();
		dice.roll();
		System.out.println(dice.toString());
		dice.roll();
		System.out.println(dice.toString());
		dice.roll();
		System.out.println(dice.toString());
		dice.roll();
		System.exit(0);
	}
}
