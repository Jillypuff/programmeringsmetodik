/**
 * 
 */
package kap4;

/**
 * Complex number calculator
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-21
 */
public class P4_10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Complex firstC = new Complex(3.0, 6.0);
		Complex secondC = new Complex(1.0, -5.0);
		System.out.println(firstC.add(secondC).toString());
		System.out.println(firstC.sub(secondC).toString());
		System.out.println(firstC.mul(secondC).toString());
		System.out.println(firstC.mul(6.0).toString());
		System.out.println(firstC.div(secondC).toString());
		System.out.println(firstC.toString());
		
		System.exit(0);
	}

}
