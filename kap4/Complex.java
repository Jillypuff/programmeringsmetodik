package kap4;

public class Complex {
	double real;
	double imaginary;
	
	public Complex() {}
	
	public Complex(double r, double i) {
		real = r;
		imaginary = i;
	}
	
	public double getReal() {
		return real;
	}
	public double getImaginary() {
		return imaginary;
	}
	public Complex add(Complex other) {
		return new Complex(real + other.real, imaginary + other.imaginary);
	}
	public Complex sub(Complex other) {
		return new Complex(real - other.real, imaginary - other.imaginary);
	}
	public Complex mul(Complex other) {
		return new Complex(real * other.real - imaginary * other.imaginary,
				real * other.imaginary + imaginary * other.real);
	}
	public Complex mul(double d) {
		return mul(new Complex(d, 0.0));
	}
	public Complex div(Complex other) {
		double numeratorReal = real * other.real + imaginary * other.imaginary;
		double numeratorImaginary = (real * other.imaginary - imaginary * other.real)*-1;
		double denominator = Math.pow(other.real, 2) + Math.pow(other.imaginary, 2);
		return new Complex(numeratorReal / denominator, numeratorImaginary / denominator);
	}

	public String toString() {
		return String.valueOf(real) + (imaginary < 0 ? " - " : " + ") + String.valueOf(Math.abs(imaginary)) + "i";
	}
	public double length() {
		return Math.sqrt(Math.pow(real, 2) + Math.pow(imaginary, 2));
	}
}
