package kap4;

public class CheckNumber{
	private double tal1;
	private double tal2;
	private double tal3;
	
	public CheckNumber() {}
	
	public void setTal1(double x) {
		tal1 = x;
	}
	public void setTal2(double x) {
		tal2 = x;
	}
	public void setTal3(double x) {
		tal3 = x;
	}
	
	public boolean allTheSame() {
		return (tal1 == tal2 && tal1 == tal3);
	}
	public boolean allDifferent() {
		return (tal1 != tal2 && tal1 != tal3 && tal2 != tal3);
	}
	public boolean sorted() {
		return (tal1 <= tal2 && tal2 <= tal3 || tal1 >= tal2 && tal2 >= tal3);
	}
}
