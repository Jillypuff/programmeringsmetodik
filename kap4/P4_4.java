/**
 * 
 */
package kap4;

/**
 * Create a tallycounter that can increase, reset, undo and get value.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-19
 */
public class P4_4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TallyCounter1 tc = new TallyCounter1();
		
		tc.count();
		tc.count();
		tc.undo();
		System.out.println(tc.getValue());
		System.out.println(tc);
		
		System.exit(0);
	}

}
