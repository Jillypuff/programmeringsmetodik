/**
 * 
 */
package kap4;

/**
 * Store a string in a class to return whole string, return middle of stirng
 * and return amount of vowels.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-19
 */
public class P4_2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create instance of CheckString with a string value
		CheckString cs = new CheckString("Pineapples");
		
		//Call the methods in CheckString and return different facts based on the string.
		System.out.format("Our string is: \"%s\".\n", cs.getString());
		System.out.format("The middle of our string is: \"%s\".\n", cs.middle());
		System.out.format("Our string contains %d vowels.", cs.countVowels());
		System.exit(0);
	}
}
