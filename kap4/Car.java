package kap4;

public class Car {
	final private double GASTANK_VOLUME = 55.0;
	private double gasLevel = 0.0;
	private double fuelPer10Km = 0.0;

	public Car(double fuelEfficiency) {
		fuelPer10Km = fuelEfficiency;
	}

	public double getGasLevel() {
		return gasLevel;
	}
	public void addFuel(double amountOfFuel) {
		gasLevel = Math.min(GASTANK_VOLUME, gasLevel + amountOfFuel);
	}
	public void drive(double scandinavianMiles) {
		double gasRequired = Math.abs(scandinavianMiles) * fuelPer10Km;
		gasLevel = Math.max(0, gasLevel - gasRequired);
	}
}
