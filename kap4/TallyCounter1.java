package kap4;

public class TallyCounter1 {
	private int counter = 0;

	public TallyCounter1() { }

	public void count() {
		counter++;
	}
	public void reset() {
		counter = 0;
	}
	public void undo() {
		counter = Math.max(0, counter-1);
	}
	public int getValue() {
		return counter;
	}
}
