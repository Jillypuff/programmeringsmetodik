/**
 * 
 */
package kap4;

/**
 * A class to store and calculate average quiz score for students.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-19
 */
public class P4_6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Student stu = new Student("Jones");
		
		System.out.println(stu);
		
		System.exit(0);
	}
}
