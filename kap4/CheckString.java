package kap4;

public class CheckString {
	private String str = "";
	
	//Permanently sets the value of string when the object of the class is created.
	public CheckString(String str) {
		this.str = str;
	}
	
	//Getter
	public String getString() {
		return str;
	}
	
	//Return middle or middle 2 of string
	public String middle() {
		int middle = str.length() / 2;
		if (str.length() % 2 == 0)
			return str.substring(middle-1, middle+1);
		else
			return str.substring(middle, middle+1);
	}
	
	//Return int = amount of vowels in string
	public int countVowels() {
		String vowels = "AOUEÅÄÖYIaoueåäöyi";
		int totalVowels = 0;
		for(int i = 0; i < str.length(); i++) {
			if(vowels.contains(Character.toString(str.charAt(i)))) {
				totalVowels++;
			}
		}
		return totalVowels;
	}
}