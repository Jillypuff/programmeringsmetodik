package kap4;

public class Bug {
	private int position = 0;
	private int direction = 1;
	
	public Bug(int startPosition) {
		position = startPosition;
	}
	public void turn() {
		direction *= -1;
	}
	public void move() {
		position += direction;
	}
	public int getPosition() {
		return position;
	}
}
