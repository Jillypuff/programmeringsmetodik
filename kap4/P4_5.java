/**
 * 
 */
package kap4;

/**
 * Same tallycounter but with an added adjustable upper limit.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-19
 */
public class P4_5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TallyCounter2 tc2 = new TallyCounter2();
		
		System.out.println(tc2);
		
		System.exit(0);
	}
}
