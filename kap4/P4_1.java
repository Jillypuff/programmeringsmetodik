/**
 * 
 */
package kap4;

/**
 * Check if 3 numbers are all the same, all different and
 * if they are in either ascending or descending order.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-19
 */

public class P4_1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create insance of CheckNumber and enter values
		CheckNumber cn = new CheckNumber();
		cn.setTal1(2);
		cn.setTal2(2);
		cn.setTal3(2);
		
		//Call the methods in CheckNumbers and return true or false
		System.out.format("All the same:\t %b\n", cn.allTheSame());
		System.out.format("All different:\t %b\n", cn.allDifferent());
		System.out.format("All sorted:\t %b", cn.sorted());
		
		System.exit(0);
	}
}
