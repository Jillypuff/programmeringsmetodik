/**
 * 
 */
package kap4;

/**
 * Create a class Car that can drive and check fuel before and
 * after dependant on fuel efficiency.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-19
 */
public class P4_7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Car car = new Car(0.55);
		
		car.addFuel(30.0);
		System.out.println(car.getGasLevel());
		car.drive(-0.5);
		System.out.println(car.getGasLevel());
		
		System.exit(0);
	}
}
