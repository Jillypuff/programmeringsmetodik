/**
 * 
 */
package kap3;

import java.util.Scanner;

/**
 * Compares three integers if they are in rising or decending order or not.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-13
 */
public class P3_3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and variables we need
		Scanner input = new Scanner(System.in);
		int int1 = input.nextInt();
		int int2 = input.nextInt();
		int int3 = input.nextInt();
		
		//Check if rising or decending or not
		if(int1 <= int2 && int2 <= int3 || int1 >= int2 && int2 >= int3) {
			System.out.println("The numbers are in order.");
		} else {
			System.out.println("The numbers are not in order.");
		}
		
		//Clean up and exit
		input.close();
		System.exit(0);
		
	}

}
