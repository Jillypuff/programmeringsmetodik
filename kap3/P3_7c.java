/**
 * 
 */
package kap3;

import java.util.Scanner;

/**
 * Takes any amount of inputs and gives back the total sum of all the inputs.
 * Stop counting when 0 is entered.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-13
 */
public class P3_7c {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and variables needed
		Scanner scan = new Scanner(System.in);
		int total_sum = 0;
		int input = 0;
		String output = "";
		
		//Run until input is 0 and add on all numbers to sum.
		while(true){
			input = scan.nextInt();
			if(input == 0) break;
			total_sum += input;
			output += String.valueOf(total_sum) + " ";
		}
		System.out.print(output);
		
		//Clean up and exit
		scan.close();
		System.exit(0);
	}
}
