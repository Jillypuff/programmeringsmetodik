/**
 * 
 */
package kap3;

import java.util.Scanner;

/**
 * Count how many vowels are in a string.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-13
 */
public class P3_8d {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and variables needed
		Scanner scan = new Scanner(System.in);
		String input = scan.nextLine();
		String vowels = "AOUEÅÄÖYIaoueåäöyi";
		int total_vowels = 0;
		
		//Checks for each character in the string if it is contained in our vowels string
		for(int i = 0; i < input.length(); i++) {
			if(vowels.contains(Character.toString(input.charAt(i)))) {
				total_vowels++;
			}
		}
		
		System.out.format("There was %d vowels in that sentence.", total_vowels);
		
		//Clean up and exit
		scan.close();
		System.exit(0);
	}
}
