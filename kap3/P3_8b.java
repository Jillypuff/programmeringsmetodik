/**
 * 
 */
package kap3;

import java.util.Scanner;

/**
 * Take a string as input and output every other char in that string.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-13
 */
public class P3_8b {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner
		Scanner scan = new Scanner(System.in);
		String input = scan.nextLine();
		String new_string = "";
		
		//Print ever other character
		for(int i = 0; i < input.length(); i++) {
			if(i%2 == 0)
				new_string += input.charAt(i);
		}
		
		System.out.println(new_string);
				
		//Clean up and exit
		scan.close();
		System.exit(0);
	}
}
