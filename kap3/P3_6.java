/**
 * 
 */
package kap3;

import java.util.Scanner;

/**
 * Give back the sum of all integers between a starting point and an end point.
 * Include starting point and end point only when they are odd.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-13
 */
public class P3_6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and start stop variable
		Scanner input = new Scanner(System.in);
		int start = input.nextInt();
		int stop = input.nextInt();
		int total_sum = 0;
		
		//Add start or stop depending if they are odd or not
		if(start%2 == 1) total_sum += start;
		if(stop%2 == 1) total_sum += stop;
		
		//Calculate sum between start and stop
		for(int i = (Math.min(start, stop)+1); i < Math.max(start, stop); i++) {
			total_sum += i;
		}
		
		System.out.format("The total sum is: %d", total_sum);
		
		input.close();
		System.exit(0);
	}
}
