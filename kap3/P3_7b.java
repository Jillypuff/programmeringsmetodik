/**
 * 
 */
package kap3;

import java.util.Scanner;

/**
 * Take in any amount of numbers and calculate amount of odd and amount of even inputs.
 * Stop program when 0 is entered.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-13
 */
public class P3_7b {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and variables needed
		Scanner scan = new Scanner(System.in);
		int evens = 0;
		int odds = 0;
		int input = 0;
		
		//Run until 0 and keep track on amount of even and odd inputs.
		while(true){
			input = scan.nextInt();
			if(input == 0) break;
			if(input%2 == 0) evens++;
			else odds++;
		}
		
		System.out.format("Even inputs: %d, odd inputs: %d", evens, odds);
		
		//Clean up and exit
		scan.close();
		System.exit(0);
	}

}
