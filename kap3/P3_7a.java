/**
 * 
 */
package kap3;

import java.util.Scanner;

/**
 * Take in any amount of numbers and give back the biggest and lowest.
 * Stop counting when a 0 is entered.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-13
 */
public class P3_7a {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and min, max variable
		Scanner scan = new Scanner(System.in);
		int input = scan.nextInt();
		int min = input;
		int max = input;
		
		//Loop until input is 0 and update min and max each loop
		while(input != 0) {
			min = Math.min(min, input);
			max = Math.max(max, input);
			input = scan.nextInt();
		}
		
		System.out.format("Min: %d, Max: %d", min, max);
		
		//Clean up and exit
		scan.close();
		System.exit(0);
	}

}
