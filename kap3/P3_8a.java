/**
 * 
 */
package kap3;

import java.util.Scanner;

/**
 * Takes a string as input and outputs all the uppcase chars of that string.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-13
 */
public class P3_8a {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner
		Scanner scan = new Scanner(System.in);
		String input = scan.nextLine();
		String input_Uppercase = "";
		
		for(int i = 0; i < input.length(); i++) {
			if(Character.isUpperCase(input.charAt(i))) {
				input_Uppercase += input.charAt(i);
			}
		}
		
		System.out.println(input_Uppercase);
		
		//Clean up and exit
		scan.close();
		System.exit(0);
	}
}
