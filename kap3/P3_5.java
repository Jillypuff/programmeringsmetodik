/**
 * 
 */
package kap3;

import java.util.Scanner;

/**
 * Take in three names and sort them.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-13
 */
public class P3_5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and our variables
		Scanner input = new Scanner(System.in);
		System.out.print("Enter three names: ");
		String name1 = input.next();
		String name2 = input.next();
		String name3 = input.next();
		
		//Write the names in alphabetically order
		if(name1.compareToIgnoreCase(name2) < 0 && name1.compareToIgnoreCase(name3) < 0) {
			System.out.println(name1);
			if(name2.compareToIgnoreCase(name3) < 0) {
				System.out.println(name2);
				System.out.println(name3);
			} else {
				System.out.println(name3);
				System.out.println(name2);
			}
		} else if (name2.compareToIgnoreCase(name3) < 0) {
			System.out.println(name2);
			if(name1.compareToIgnoreCase(name3) < 0) {
				System.out.println(name1);
				System.out.println(name3);
			} else {
				System.out.println(name3);
				System.out.println(name1);
			}
		} else {
			System.out.println(name3);
			if (name1.compareToIgnoreCase(name2) < 0) {
				System.out.println(name1);
				System.out.println(name2);
			} else {
				System.out.println(name2);
				System.out.println(name1);
			}
		}
		
		//Clean up and exit
		input.close();
		System.exit(0);
		
	}

}
