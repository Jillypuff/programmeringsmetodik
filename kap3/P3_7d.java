/**
 * 
 */
package kap3;

import java.util.Scanner;

/**
 * Take any amount of numbers and write our each one that pairs up with the one before.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-13
 */
public class P3_7d {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and min, max variable
		Scanner scan = new Scanner(System.in);
		int input = 0;
		int last_input = 0;
		String all_pairs = "";
		
		//Loop until input is 0 and add the value to all_pairs if matching pair found.
		while(true) {
			input = scan.nextInt();
			if(input == 0) break;
			if(input == last_input) {
				all_pairs += String.valueOf(input) + " ";
				last_input = 0;
			} else {
				last_input = input;
			}
		}
		
		System.out.println(all_pairs);
		
		//Clean up and exit
		scan.close();
		System.exit(0);
	}
}
