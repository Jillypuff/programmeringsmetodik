/**
 * 
 */
package kap3;

import java.util.Scanner;

/**
 * Replace all vowels in a string with an underscore.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-13
 */
public class P3_8c {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and variables needed
		Scanner scan = new Scanner(System.in);
		String input = scan.nextLine();
		String vowels = "AOUEÅÄÖYIaoueåäöyi";
		
		//Checks for each character in the string if it is contained in our vowels string
		for(int i = 0; i < input.length(); i++) {
			if(vowels.contains(Character.toString(input.charAt(i)))) {
				System.out.print("_");
			} else {
				System.out.print(input.charAt(i));
			}
		}
		
		//Clean up and exit
		scan.close();
		System.exit(0);
	}
}
