/**
 * 
 */
package kap3;

import java.util.Scanner;

/**
 * Convert an int to binary
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-14
 */
public class P3_10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and variables needed
		Scanner scan = new Scanner(System.in);
		int input = scan.nextInt();
		String binary = "";
		
		//Convert to binary
		do {
			binary = String.valueOf(input%2) + binary;
			input /= 2;
		} while(input > 0);
		
		System.out.println(binary);
		
		//Clean up and exit
		scan.close();
		System.exit(0);
	}

}
