/**
 * 
 */
package kap3;

import java.util.Scanner;

/**
 * Ta en input och ge tillbaka om den är negativ, lika med noll eller positiv.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-12
 */
public class P3_1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and the variable we will use
		Scanner input = new Scanner(System.in);
		int number = input.nextInt();
		
		//Check if the variable is < 0 <.
		if (number < 0) {
			System.out.format("%d är negativt.", number);
		} else if (number == 0) {
			System.out.format("%d är noll.", number);
		} else {
			System.out.format("%d är positivt.", number);
		}
		
		//Clean up and exit.
		input.close();
		System.exit(0);
	}

}
