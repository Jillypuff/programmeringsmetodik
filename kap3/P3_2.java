/**
 * 
 */
package kap3;

import java.util.Scanner;

/**
 * Compares three integers if there similar of not.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-13
 */
public class P3_2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and the variables we're going to compare
		Scanner input = new Scanner(System.in);
		int int1 = input.nextInt();
		int int2 = input.nextInt();
		int int3 = input.nextInt();
		
		//Compare the integers
		if(int1 == int2 && int1 == int3) {
			System.out.println("The number are all the same.");
		} else if(int1 != int2 && int1 != int3 && int2 != int3) {
			System.out.println("The numbers are all different.");
		} else {
			System.out.println("The numbers are neither all same nor all different.");
		}
		
		//Clean up and exit.
		input.close();
		System.exit(0);
	}

}
