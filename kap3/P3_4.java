/**
 * 
 */
package kap3;

import java.util.Scanner;

/**
 * Check if water is gas, normal or frozen at different temps and heights.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-13
 */
public class P3_4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and variables we need
		Scanner input = new Scanner(System.in);
		System.out.print("Degrees: ");
		String degrees = input.nextLine();
		System.out.print("Height: ");
		String height = input.nextLine();
		
		//Set heat and convert to celsius if it's fahrenheit
		double heat = Double.parseDouble(degrees.substring(0, degrees.length()-2));
		if(degrees.substring(degrees.length()-1).equals("F")) {
			heat = (heat-32)/1.8;
		}
		
		//Set altitude and convert to meter if it's feet
		double altitude = Double.parseDouble(height.substring(0, height.length()-2));
		if(height.substring(height.length()-1).equals("f")) {
			altitude /= 3.2808;
		}

		//Check for different heat breakpoints
		if(heat <= 0) {
			System.out.println("The water is solid.");
		} else if(heat <= (double)(100 - (altitude/300))) {
			System.out.println("The water is liquid.");
		} else {
			System.out.println("The water is gaseous.");
		}
		
		//Clean up and exit
		input.close();
		System.exit(0);
	}

}
