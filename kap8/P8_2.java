/**
 * 
 */
package kap8;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Create a new version of existing file but with line counters.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-19
 *
 */
public class P8_2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String inputFileName = getFileName("Enter input file name: ");
		String outputFileName = getFileName("Enter output file name: ");	
		Scanner reader = readToScanner(inputFileName);
		writeToFile(outputFileName, reader);
		System.exit(0);
		
	}
	
	public static String getFileName(String prompt) {
		Scanner input = new Scanner(System.in);
		System.out.print(prompt);
		String returnString = input.nextLine();
		input.close();
		return returnString;
	}
	
	public static String getFileName() {
		Scanner input = new Scanner(System.in);
		System.out.print("Enter name of file: ");
		String returnString = input.nextLine();
		input.close();
		return returnString;
	}
	
	public static Scanner readToScanner(String fileName) {
		Scanner reader = null;
		try {
			reader = IOStatic.readToScanner(fileName);
		} catch (IOException e) {
			System.out.println("Cannot find or read from " + fileName + ".");
			System.exit(1);
		}
		return reader;
	}
	
	public static void writeToFile(String fileName, Scanner reader) {
		try {
			FileWriter writer = new FileWriter(fileName);
			int lineNumber = 1;
			while(reader.hasNextLine()) {
				writer.write("/* " + lineNumber + " */ " + reader.nextLine() + "\n");
				lineNumber++;
			}
			reader.close();
			writer.close();
		} catch (IOException e) {
			System.out.println("Could not write to " + fileName + ".");
			System.exit(1);
		}
		
	}
}
