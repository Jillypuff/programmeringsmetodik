/**
 * 
 */
package kap8;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-19
 *
 */
public class P8_9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Enter name of input file: ");
		String inputFileName = input.nextLine();
		System.out.print("Enter name of new file: ");
		String outputFileName = input.nextLine();
		System.out.print("Enter how many steps the encryption should shift: ");
		int offset = input.nextInt();
		input.close();

		String fullText = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(inputFileName));
			String line;
			while((line = br.readLine()) != null) {
				fullText += line + "\n";
			}
			fullText = fullText.substring(0, fullText.length() - 1);
			br.close();
		} catch (IOException e) {
			throw new InputMismatchException("Could not find or read from " + inputFileName + ".");
		}

		try {
			IOStatic.writeToFile(outputFileName, caesarCrypt(fullText, offset));
		} catch (IOException e) {
			System.out.println("Cannot write to " + outputFileName + ".");
			System.exit(1);
		}
		
		System.exit(0);
	}
	
	public static String caesarCrypt(String text, int offsetKey){
		byte[] unicodeChars = null;
		try {
			unicodeChars = text.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			System.out.println("Problems with UTF-8 encoding.");
			System.exit(1);
		}
		byte newUnicodeChar;
		for(int i = 0; i < unicodeChars.length; i++) {
			if(unicodeChars[i] >= 65 && unicodeChars[i] <= 90) {
				newUnicodeChar = (byte) (unicodeChars[i] + offsetKey);
				if(newUnicodeChar > 90)
					newUnicodeChar -= 26;
				if(newUnicodeChar < 65)
					newUnicodeChar += 26;
			}
			else if(unicodeChars[i] >= 97 && unicodeChars[i] <= 122){
				newUnicodeChar = (byte) (unicodeChars[i] + offsetKey);
				if(newUnicodeChar > 122)
					newUnicodeChar -= 26;
				if(newUnicodeChar < 97)
					newUnicodeChar += 26;
			} else {
				newUnicodeChar = unicodeChars[i];
			}
			unicodeChars[i] = newUnicodeChar;
		}
		String returnString = null;
		try {
			returnString = new String(unicodeChars, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			System.out.println("Problems with UTF-8 encoding.");
			System.exit(1);
		}
		
		return returnString;

	}
}
