/**
 * 
 */
package kap8;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Create a new version of existing file but with line counters.
 * Can be ran either normally or with 2 arguments as input and output files.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-19
 *
 */
public class P8_3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String inputFileName;
		String outputFileName;
		
		if(args.length == 1 || args.length > 2) {
			System.out.println("Must be called with 0 or 2 arguments.");
			System.exit(1);
		}
		if(args.length == 0) {
			Scanner input = new Scanner(System.in);
			System.out.print("Enter file you want to read from: ");
			inputFileName = input.nextLine();
			System.out.print("Enter file you want to write to: ");
			outputFileName = input.nextLine();
			input.close();
		} else {
			inputFileName = args[0];
			outputFileName = args[1];
		}
		
		Scanner reader = null;
		try {
			reader = IOStatic.readToScanner(inputFileName);
		} catch (IOException e) {
			System.out.println("Cannot find or read from " + inputFileName + ".");
			System.exit(1);
		}
		
		try {
			FileWriter writer = new FileWriter(outputFileName);
			int lineNumber = 1;
			while(reader.hasNextLine()) {
				writer.write("/* " + lineNumber + " */ " + reader.nextLine() + "\n");
				lineNumber++;
			}
			reader.close();
			writer.close();
		} catch (IOException e) {
			System.out.println("Could not write to " + outputFileName + ".");
			System.exit(1);
		}
		
		System.exit(0);
		
	}
}
