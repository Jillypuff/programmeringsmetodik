/**
 * 
 */
package kap8;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Läs av antal rader, ord och tecken från en textfil.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-19
 *
 */
public class P8_5 {

	/**
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException{
		String inputFileName;
		Scanner input = new Scanner(System.in);
		System.out.print("Enter name of file: ");
		inputFileName = input.nextLine();
		input.close();

		String fullText = null;
		try {
			fullText = IOStatic.readToString(inputFileName);
		} catch (IOException e) {
			throw new FileNotFoundException("Cannot find or read from " + inputFileName + ".");
		}
		int lines = (int) fullText.lines().count();
		int words = 0;
		int characters = fullText.replaceAll("\\n|\\r", "").length();
		
		if(!fullText.isBlank()) {
			String[] amountOfWords = fullText.split("\\s+|\\n|\\r", 0);
			words += amountOfWords.length;
		}
		
		System.out.println("Amount of rows: " + lines);
		System.out.println("Amount of words: " + words);
		System.out.println("Amount of characters: " + characters);
		
		System.exit(0);
	}
}
