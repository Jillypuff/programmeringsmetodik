/**
 * 
 */
package kap8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Static class to handle all my needs for creating, reading and
 * writing to files.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-19
 *
 */
public class IOStatic {
	
	public IOStatic() {}
	
	public static void createFile(String fileName) throws IOException {
		File file = new File(fileName);
		if(file.createNewFile())
			System.out.println(file.getName() + " has been created.");
		else
			System.out.println(fileName + " already exists.");
	}
	
	public static File openFile(String fileName) {
		File file = new File(fileName);
		return file;
	}
	
	public static void writeToFile(String fileName, String text) throws IOException {
		writeToFile(fileName, text, false);
	}	
	public static void writeToFile(String fileName, String text, boolean append) throws IOException {
		FileWriter writer = new FileWriter(fileName, append);
		writer.write(text);
		writer.close();
	}
	
	public static void readFile(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		String line;
		while((line = br.readLine()) != null) {
			System.out.println(line);
		}
		br.close();
	}
	
	public static Scanner readToScanner(String fileName) throws IOException {
		Scanner reader = new Scanner(openFile(fileName));
		return reader;
	}
	
	public static String readToString(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		String returnString = "";
		String line;
		while((line = br.readLine()) != null) {
			returnString += line + "\n";
		}
		br.close();
		if(returnString.length() == 0) {
			return "";
		}
		return returnString.substring(0, returnString.length() - 1);
	}
}
