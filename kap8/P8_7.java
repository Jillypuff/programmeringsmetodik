/**
 * 
 */
package kap8;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-17
 *
 */
public class P8_7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String prompt = "Enter a float number: ";
		double sum = 0;
		boolean wrongInput = false;
		while(true) {
			try {
				sum += readDouble(input, prompt);
				wrongInput = false;
			} catch (InputMismatchException e){
				if(wrongInput)
					break;
				input.next();
				wrongInput = true;
			}
		}
		System.out.println("\n" + sum);
		System.exit(0);
	}
	
	public static double readDouble(Scanner in, String prompt) throws InputMismatchException{
		System.out.print(prompt);
		double input = in.nextDouble();
		return input;
	}
}
