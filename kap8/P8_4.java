/**
 * 
 */
package kap8;

import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Read a file of numbers separeted by rows and columns.
 * Gives back the average value of each of the columns.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-19
 *
 */
public class P8_4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String inputFileName;
		
		Scanner input = new Scanner(System.in);
		System.out.println("Enter file you want to get average from: ");
		inputFileName = input.nextLine();
		input.close();
		
		Scanner reader = null;
		
		try {
			reader = IOStatic.readToScanner(inputFileName);
		} catch (IOException e) {
			System.out.println("Cannot find or read from " + inputFileName + ".");
			System.exit(1);
		}
		
		double[] columnAverage = getAvgCols(reader);
		reader.close();
		
		System.out.println("Medelvärdet för kolumn 1: " + columnAverage[0]);
		System.out.println("Medelvärdet för kolumn 2: " + columnAverage[1]);
		
		System.exit(0);
	}
	
	public static double[] getAvgCols(Scanner reader) {
		double col1 = 0.0;
		double col2 = 0.0;
		int numbersInCol1 = 0;
		int numbersInCol2 = 0;
		while(reader.hasNextLine()) {
			String temp = reader.nextLine();
			if(Pattern.matches("(^[0-9,.]+)[\\s]+([0-9.,]+)", temp)) {
				String[] tempArr = temp.replaceAll(",", ".").trim().split("\\s+", 0);
				col1 += Double.valueOf(tempArr[0]);
				col2 += Double.valueOf(tempArr[1]);
				numbersInCol1++;
				numbersInCol2++;
			} else if (Pattern.matches("(^[0-9,.]+)", temp)){
				col1 += Double.valueOf(temp.replaceAll(",", "."));
				numbersInCol1++;
			} else if (Pattern.matches("(^[\\s]+)([0-9.,]+)", temp)){
				col2 += Double.valueOf(temp.replaceAll(",", "."));
				numbersInCol2++;
			} else {
				System.out.println("Error: File contains something other than numbers.");
			}
		}
		return new double[] {col1/numbersInCol1, col2/numbersInCol2};
	}
}
