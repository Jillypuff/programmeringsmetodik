/**
 * 
 */
package kap8;

import java.io.IOException;
import java.util.Scanner;

/**
 * Vänder på all text per rad i en textfil. Sparar till samma
 * fil som det läses ifrån.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-19
 *
 */
public class P8_6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String inputFileName;
		Scanner input = new Scanner(System.in);
		System.out.print("Enter name of input file: ");
		inputFileName = input.nextLine();
		input.close();
		

		String fullText = "";
		try {
			fullText = IOStatic.readToString(inputFileName);
		} catch (IOException e) {
			System.out.println("Cannot find or read from " + inputFileName + ".");
			System.exit(1);
		}
		
		String backwardsText = "";
		String[] lineByLine = fullText.split("\n", 0);
		for(int i = 0; i < lineByLine.length ; i++) {
			for(int j = lineByLine[i].length() - 1; j >= 0; j--) {
				backwardsText += lineByLine[i].charAt(j);
			}
			backwardsText += "\n";
		}
		backwardsText = backwardsText.substring(0, backwardsText.length()-1);
		
		try {
			IOStatic.writeToFile(inputFileName, backwardsText);
		} catch (IOException e) {
			System.out.println("Cannot find or write to " + inputFileName + ".");
			System.exit(1);
		}
		
		System.exit(0);
		
	}
}
