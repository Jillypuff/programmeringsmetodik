/**
 * 
 */
package kap8;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

/**
 * 
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-17
 *
 */
public class P8_8 {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Scanner input = new Scanner(System.in);
		System.out.print("Enter a website url: ");
		String adress = input.nextLine();
		System.out.print("Enter name of output file: ");
		String fileName = input.nextLine();
		input.close();
		
		Scanner sc = new Scanner(new URL(adress).openStream());
		
		String urlText = "";
		while(sc.hasNext()) {
			urlText += sc.nextLine();
			urlText += "\n";
		}
		sc.close();
		if(!urlText.isBlank())
			urlText = urlText.substring(0, urlText.length()-1);
		IOStatic.writeToFile(fileName, urlText);
		
		System.exit(0);
	}

}
