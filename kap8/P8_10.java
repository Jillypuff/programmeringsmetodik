/**
 * 
 */
package kap8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Convert java field to html file with <strong> marked keywords.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-18
 *
 */
public class P8_10 {

	/**
	 * @param args
	 */
	public static void main(String[] args){
		String javaKeywords = "JavaKeywords.csv";
		String inputFileName;
		String outputFileName;
		
		Scanner input = new Scanner(System.in);
		System.out.print("Enter name of input file: ");
		inputFileName = input.nextLine();
		System.out.print("Enter name of new file: ");
		outputFileName = input.nextLine();
		input.close();
		if(!outputFileName.contains(".html"))
			outputFileName += ".html";
		
		File inputFile = IOStatic.openFile(inputFileName);
		File outputFile = IOStatic.openFile(outputFileName);
		File keywords = IOStatic.openFile(javaKeywords);
		
		try {
			buildHtmlPage(inputFile, outputFile, keywords);
		} catch (FileNotFoundException e) {
			System.out.println(inputFile + " was not found.");
			System.exit(1);
		}

		System.exit(0);
	}
	
	public static void buildHtmlPage(File input, File output, File keyword) throws FileNotFoundException {
		String htmlStart = "<!DOCTYPE html>\n<html>\n<body>\n<pre>\n";
		String htmlEnd = "</pre>\n</body>\n</html>";
		String html = "";
		String[] keywords = null;
		
		try {
			keywords = extractCvs(keyword);
		} catch (FileNotFoundException e) {
			System.out.println(keyword.getName() + " could not be found.");
			System.exit(1);
		}
		
		BufferedReader br = new BufferedReader(new FileReader(input));
		String line = "";
		try {
			while((line = br.readLine()) != null) {
				html += addStrong(keywords, line) + "\n";
			}
		} catch (IOException e) {
			System.out.println("Cannot read from " + input.getName() + ".");
			System.exit(1);
		}
		
		html = htmlStart + html + htmlEnd;
		
		try {
			IOStatic.writeToFile(output.getName(), html);
		} catch (IOException e) {
			System.out.println("Cannot find or write to " + output.getName() + ".");
			System.exit(1);
		}
	}
		
	public static String addStrong(String[] keywords, String line) {
		String strongOpen = "<strong>";
		String strongClose = "</strong>";
		boolean keywordFound = false;
		for(String key : keywords) {
			String regex = "(^|\\s|[}])" + key + "(\\s|$|[{])";
			int index = 0;
			do {
				int keywordIndex = indexOf(Pattern.compile(regex), line.substring(index));
				if(keywordIndex != -1) {						keywordFound = true;
					line = line.substring(0, keywordIndex) + strongOpen + key +
							strongClose + line.substring(keywordIndex + key.length());
					index = keywordIndex + 16;
				} else {
					keywordFound = false;
				}
			} while(keywordFound);
		}
		return line;
	}
		
	public static int indexOf(Pattern regex, String line) {
		Matcher matcher = regex.matcher(line);
		if(!matcher.find())
			return -1;
		if(Character.isWhitespace(line.charAt(0)))
			return matcher.start() + 1;
		else
			return matcher.start() == 0 ? 0 : matcher.start() + 1;
	}
	
	public static String[] extractCvs(File keyword) throws FileNotFoundException {
		BufferedReader br = new BufferedReader(new FileReader(keyword));
		String keywordsString = "";
		String line;
		try {
			while((line = br.readLine()) != null) {
				keywordsString += line + "\n";
			}
			br.close();
		} catch (IOException e1) {
			System.out.println("Cannot read from " + keyword.getName() + ".");
			System.exit(1);
		}
		String[] keywords = keywordsString.split(",", 0);
		return keywords;
	}
}
