/**
 * 
 */
package kap8;

import java.io.IOException;

/**
 * Create a new file, write to file then read from it.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-198
 *
 */
public class P8_1 {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args){
		String outputFileName = "hello.txt";
		String inputText = "Hello, World!";
		
		try {
			IOStatic.createFile(outputFileName);
		} catch (IOException e) {
			System.out.println("Could not create or find " + outputFileName + ".");
			System.exit(1);
		}
		
		try {
			IOStatic.writeToFile(outputFileName, inputText);
		} catch (IOException e) {
			System.out.println("Could not write to " + outputFileName + ".");
			System.exit(1);
		}
		
		try {
			IOStatic.readFile(outputFileName);
		} catch (IOException e) {
			System.out.println("Could not read from " + outputFileName + ".");
			System.exit(1);
		}
		
		System.exit(0);
	}
}
