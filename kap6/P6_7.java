/**
 * 
 */
package kap6;

/**
 * Test valid and invalids inputs for a car to check exceptions.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */
public class P6_7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		CarWithEx car = null;
		
		try {
			car = new CarWithEx();
		} catch (IllegalArgumentException e) {
			System.err.println(e.getLocalizedMessage());
			System.exit(1);
		}
		try {
			car.addFuel(30.0);
		} catch (IllegalArgumentException e) {
			System.err.println(e.getLocalizedMessage());
			System.exit(1);
		}
		System.out.println(car.getGasLevel());
		try {
			car.drive(-70.0);
		} catch (IllegalArgumentException e) {
			System.err.println(e.getLocalizedMessage());
			System.exit(1);
		}
		System.out.println(car.getGasLevel());
	}

}
