package kap6;

import java.util.Random;

/**
 * Create, rolls and get values from multiple different dices.
 * Can create without arguments to get 5, 6-sided dice or pass
 * in any amount of different correct sides to create that many
 * of that type of dice.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */

public class Yatzy2 {
	Dice2[] dice;
	
	public Yatzy2(int... a) {
		if(a.length == 0) {
			dice = new Dice2[5];
			for(int i = 0; i < 5; i++)
				dice[i] = new Dice2(6);
		} else {
			dice = new Dice2[a.length];
			for(int i = 0; i < a.length; i++)
				dice[i] = new Dice2(a[i]);
		}
	}
	
	public void roll() {
		for(Dice2 d : dice)
			d.roll();
	}
	public int getTotalValue() {
		int sum = 0;
		for(Dice2 d : dice)
			sum += d.getValue();
		return sum;
	}
	public String toString() {
		String returnString = "";
		for(Dice2 d : dice)
			returnString += d;
		return returnString.substring(0, returnString.length() - 1);
	}

	class Dice2 {
		private int value = 1;
		private int sides;
		private Random rand = new Random();
		
		public Dice2() {
			this(6);
		}
		
		public Dice2(int sides) {
			switch(sides) {
			case 4: case 6: case 8: case 10: case 12: case 20:
				this.sides = sides;
				break;
			default:
				throw new IllegalArgumentException("Dice can only be 4, 6, 8, 10, 12 or 20 sides.");
			}
		}
		
		public void roll() {
			value = rand.nextInt(sides) + 1;
		}
		public int getValue() {
			return value;
		}
		public String toString() {
			return "[" + value + "] ";
		}
	}
}
