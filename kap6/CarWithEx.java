package kap6;

/**
 * Creates a car that can fill gas and drive. Throws exceptions
 * when invalid amounts are entered.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */

public class CarWithEx {
	static final private double GASTANK_VOLUME = 55.0;
	private double gasLevel = 0.0;
	private double fuelPer10Km = 0.0;
	
	public CarWithEx() {
		this(0.5);
	}

	public CarWithEx(double fuelEfficiency) {
		if(fuelEfficiency <= 0)
			throw new IllegalArgumentException("Something is wrong with your car.");
		fuelPer10Km = fuelEfficiency;
	}

	public double getGasLevel() {
		return gasLevel;
	}
	public void addFuel(double amountOfFuel) {
		if(amountOfFuel < 0)
			throw new IllegalArgumentException("Cannot siphon gas.");
		if(gasLevel + amountOfFuel > GASTANK_VOLUME) {
			gasLevel = GASTANK_VOLUME;
			throw new IllegalArgumentException("Spilled gas, be more careful.");
		}
		gasLevel = gasLevel + amountOfFuel;
	}
	public void drive(double scandinavianMiles) {
		if(scandinavianMiles < 0)
			throw new IllegalArgumentException("You can't drive a negative amount.");
		double gasRequired = scandinavianMiles * fuelPer10Km;
		if(gasRequired > gasLevel)
			throw new IllegalArgumentException("You don't have enough gas to drive that far.");
		System.out.println(gasLevel + " " + gasRequired);
		gasLevel -= gasRequired;
	}
}
