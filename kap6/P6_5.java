/**
 * 
 */
package kap6;

/**
 * Fifth testprogram for static methods.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */
public class P6_5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int factorizeMe = 4;		
		
		System.out.println(factorizeMe + " have these factors:");
		for(int i : StaticUtilityMethods.integerFactorization(factorizeMe))
			System.out.println(i);
		
		System.exit(0);
	}

}