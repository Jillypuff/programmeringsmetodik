/**
 * 
 */
package kap6;

/**
 * Create a dice to roll, throws exception when invalid amount
 * of sides is entered.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */
public class P6_9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Dice2 dice = null;
		try {
			dice = new Dice2(15);
		} catch (IllegalArgumentException e) {
			System.err.println("Exception: " + e.getLocalizedMessage());
			System.exit(1);
		}
		for(int i = 0; i < 10; i++) {
			dice.roll();
			System.out.println(dice.toString());
		}
		System.exit(0);
	}
}
