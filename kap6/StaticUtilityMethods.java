/**
 * 
 */
package kap6;

import java.util.regex.Pattern;

/**
 * Tests for bunch of different static methods.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */
public class StaticUtilityMethods {

	private StaticUtilityMethods() {}
	
	public static int calculateSum(int a, int b) {
		return a + b;
	}

	public static int calculateSum(int a, int b, int c) {
		return calculateSum(a, b) + c;
	}

	public static double calculateSum(double a, double b) {
		return a + b;
	}

	public static double calculateSum(double a, double b, double c) {
		return calculateSum(a, b) + c;
	}

	public static int calculateSum(int[] a) {
		int sum = 0;
		for (int i : a)
			sum += i;
		return sum;
	}

	public static double calculateSum(double[] a) {
		double sum = 0;
		for (double i : a)
			sum += i;
		return sum;
	}

	public static int calculateSum(int[][] a) {
		int sum = 0;
		for (int[] i : a)
			for (int j : i)
				sum += j;
		return sum;
	}
	
	public static Boolean checkSweEmailAddress(String str) {
		return Pattern.matches("([A-ZÅÄÖa-zåäö1-9_.,!^*?=`()&%¤#\"!]+)@([A-ZÅÄÖa-zåäö1-9_.,!^*?=`()&%¤#\"!]+)[.]se", str);
	}

	public static String convertToCamelCase(String str) {
		str = str.trim().replaceAll(" +", " ");
		String[] strArray = str.split(" ", 0);
		String returnString = "";
		for (String s : strArray) {
			if (s.length() == 1)
				returnString += s.toUpperCase();
			else
				returnString += Character.toUpperCase(s.charAt(0)) + s.substring(1).toLowerCase();
		}
		return returnString;
	}

	public static int[] merge(int[] a, int[] b) {
		int[] returnArray = new int[a.length + b.length];
		int lengthDifference = Math.abs(a.length - b.length);
		for (int i = 0; i < returnArray.length - lengthDifference; i++)
			returnArray[i] = (i & 1) == 0 ? a[i / 2] : b[(i - 1) / 2];

		for (int i = 0; i < lengthDifference; i++)
			returnArray[Math.min(a.length, b.length) * 2 + i] = a.length > b.length ? a[b.length + i] : b[a.length + i];

		return returnArray;
	}

	public static String merge(String str1, String str2) {
		String returnString = "";
		str1 = str1.trim().replaceAll(" +", " ");
		str2 = str2.trim().replaceAll(" +", " ");
		String[] strArray1 = str1.split(" ", 0);
		String[] strArray2 = str2.split(" ", 0);
		int lengthDifference = Math.abs(strArray1.length - strArray2.length);
		for (int i = 0; i < strArray1.length + strArray2.length - lengthDifference; i++)
			returnString += (i & 1) == 0 ? strArray1[i / 2] + " " : strArray2[(i - 1) / 2] + " ";

		for (int i = 0; i < lengthDifference; i++)
			returnString += strArray1.length > strArray2.length ?
					strArray1[strArray2.length + i] + " " : strArray2[strArray1.length + i] + " ";

		return returnString.substring(0, returnString.length() - 1);
	}
	
	public static int[] integerFactorization(int a) {
		int[] returnArray;
		if(a < 4) return returnArray = new int[0];
		int[] smallFactorStorage = new int[100];
		int factors = 0;
		for(int i = 2; i < a / 2 + 1; i++) {
			if(a % i == 0) {
				smallFactorStorage[factors] = i;
				factors++;
			}
		}
		returnArray = new int[factors];
		for(int i = 0; i < returnArray.length; i++) {
			returnArray[i] = smallFactorStorage[i];
		}
		return returnArray;
	}
	
	public static int recursiveCumulativeSum(int i) {
		if(i < 0)
			throw new IllegalArgumentException("Need to be a positive integer.");
		if(i == 0)
			return 0;
		return i % 10 + recursiveCumulativeSum(i/10);
	}
}
