/**
 * 
 */
package kap6;

/**
 * Third testprogram for static methods.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */
public class P6_3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {	
		String testEmail = "abc@def.se.se";
		String convertString = "GODDAG I STUGAN";
		 
		System.out.println("Is " + testEmail + " is a valid swe email? " + StaticUtilityMethods.checkSweEmailAddress(testEmail));
		System.out.println("\"" + convertString + "\" in camel case is: " + StaticUtilityMethods.convertToCamelCase(convertString) + ".");
		
		System.exit(0);
	}

}