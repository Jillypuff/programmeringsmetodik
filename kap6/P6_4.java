/**
 * 
 */
package kap6;

/**
 * Forth testprogram for static methods.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */
public class P6_4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] mergeArray1 = {1, 3, 5, 7};
		int[] mergeArray2 = {2, 4, 6, 8, 9, 10};
		String mergeString1 = "One Three Five Seven";
		String mergeString2 = "Two Four Six Eight Nine Ten";
		 
		System.out.println("Our two arrays merged togheter is:");
		for(int i : StaticUtilityMethods.merge(mergeArray1, mergeArray2))
			System.out.println(i);
		System.out.println("\"" + mergeString1 + "\" merged with \"" + mergeString2 + "\" is");
		System.out.println("\"" + StaticUtilityMethods.merge(mergeString1, mergeString2) + "\"");
		
		System.exit(0);
	}

}