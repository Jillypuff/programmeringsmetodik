/**
 * 
 */
package kap6;

/**
 * Sixth testprogram for static methods.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */

public class P6_8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int i = 555;
		try {
			System.out.println("Sum of all numbers up to " + i + " is: " + StaticUtilityMethods.recursiveCumulativeSum(i));
		} catch (IllegalArgumentException e) {
			System.err.println("Exception: " + e.getLocalizedMessage());
			System.exit(1);
		}
		System.exit(0);
	}
}
