/**
 * 
 */
package kap6;

/**
 * First testprogram for static methods.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */
public class P6_1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {	
		System.out.println(StaticUtilityMethods.calculateSum(1, 2));
		System.out.println(StaticUtilityMethods.calculateSum(1, 2, 3));
		System.out.println(StaticUtilityMethods.calculateSum(1.1, 2.2));
		System.out.println(StaticUtilityMethods.calculateSum(1.1, 2.2, 3.3));
		
		System.exit(0);
	}

}
