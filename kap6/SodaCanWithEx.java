package kap6;

/**
 * Gives back different values of a can based on height and radius.
 * Throws exceptions when invalid measurements are entered.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */

public class SodaCanWithEx {
	private double height;
	private double radius;
	
	public SodaCanWithEx(double height, double radius) {
		if(height < 0|| radius < 0) {
			throw new IllegalArgumentException("Need to be positive floatvalues.");
		}
		this.height = height;
		this.radius = radius;
	}
	
	public double getHeight() {
		return height;
	}
	public double getRadius() {
		return radius;
	}
	public double getVolume() {
		return height * Math.PI * Math.pow(radius, 2);
	}
	public double getSurfaceArea() {
		return 2 * Math.PI * radius * height + 2 * Math.PI * Math.pow(radius, 2);
	}
	public double getBottomArea() {
		return Math.pow(radius, 2) * Math.PI;
	}
	public double getCircumference() {
		return 2 * Math.PI * radius;
	}
}
