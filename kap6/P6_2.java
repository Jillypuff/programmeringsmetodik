/**
 * 
 */
package kap6;

/**
 * Second testprogram for static methods.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */
public class P6_2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {	
		int[] intArray = {1, 2, 3 , 4, 5};
		double[] doubleArray = {1.1, 2.2, 3.3, 4.4, 5.5};
		int[][] int2DArray = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
		 
		System.out.println("Sum of intArray: " + StaticUtilityMethods.calculateSum(intArray));
		System.out.println("Sum of doubleArray: " + StaticUtilityMethods.calculateSum(doubleArray));
		System.out.println("Sum of int2DArray : " + StaticUtilityMethods.calculateSum(int2DArray));	
		
		System.exit(0);
	}

}
