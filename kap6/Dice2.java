package kap6;

import java.util.Random;

/**
 * Create a dice to roll, throws exception when invalid amount
 * of sides is entered.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */

public class Dice2 {
	private int value = 1;
	private int sides;
	private Random rand = new Random();
	
	public Dice2() {
		this(6);
	}
	
	public Dice2(int sides) {
		switch(sides) {
		case 4: case 6: case 8: case 10: case 12: case 20:
			this.sides = sides;
			break;
		default:
			throw new IllegalArgumentException("Dice can only be 4, 6, 8, 10, 12 or 20 sides.");
		}
	}
	
	public void roll() {
		value = rand.nextInt(sides) + 1;
	}
	public int getValue() {
		return value;
	}
	public String toString() {
		return "[" + value + "]";
	}
}
