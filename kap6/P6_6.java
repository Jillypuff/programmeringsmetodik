/**
 * 
 */
package kap6;

/**
 * Gives back different values of a can based on height and radius.
 * Throws exceptions when invalid measurements are entered.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */
public class P6_6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		double height = 6.3;
		double radius = -1;
		SodaCanWithEx sodaCan = null;
		try {
			sodaCan = new SodaCanWithEx(height, radius);
		} catch (IllegalArgumentException e) {
			System.err.println("Exception: " + e.getLocalizedMessage());
			System.exit(1);
		}
		
		
		System.out.println(sodaCan.getBottomArea());
	}
}
