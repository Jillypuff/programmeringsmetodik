/**
 * 
 */
package kap6;

/**
 * Test all parts of static methods all at once.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */
public class TestAll {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] intArray = {1, 2, 3 , 4, 5};
		double[] doubleArray = {1.1, 2.2, 3.3, 4.4, 5.5};
		int[][] int2DArray = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
		String testEmail1 = "abc@def.se";
		String testEmail2 = "abc@def.com";
		String convertString = "  GODDAG   I STUGAN  ";
		int[] mergeArray1 = {1, 3, 5, 7, 9};
		int[] mergeArray2 = {2, 4, 6, 8, 10, 11, 12, 13};
		String mergeString1 = "One Three Five Seven";
		String mergeString2 = "Two Four Six Eight Nine Ten";
		int factorizeMe = 3340;		
		int comulativeSum = 123456789;
		 
		System.out.println("1 + 2 = " + StaticUtilityMethods.calculateSum(1, 2));
		System.out.println("1 + 2 + 3 = " + StaticUtilityMethods.calculateSum(1, 2, 3));
		System.out.println("1.1 + 2.2 = " + StaticUtilityMethods.calculateSum(1.1, 2.2));
		System.out.println("1.1 + 2.2 + 3.3 = " + StaticUtilityMethods.calculateSum(1.1, 2.2, 3.3));
		System.out.println("Sum of intArray: " + StaticUtilityMethods.calculateSum(intArray));
		System.out.println("Sum of doubleArray: " + StaticUtilityMethods.calculateSum(doubleArray));
		System.out.println("Sum of int2DArray : " + StaticUtilityMethods.calculateSum(int2DArray));
		System.out.println("Is " + testEmail1 + " is a valid swe email? " + StaticUtilityMethods.checkSweEmailAddress(testEmail1));
		System.out.println("Is " + testEmail2 + " is a valid swe email? " + StaticUtilityMethods.checkSweEmailAddress(testEmail2));
		System.out.println("\"" + convertString + "\" in camel case is: " + StaticUtilityMethods.convertToCamelCase(convertString) + ".");
		System.out.println("Our two arrays merged togheter is:");
		for(int i : StaticUtilityMethods.merge(mergeArray1, mergeArray2))
			System.out.println(i);
		System.out.println("\"" + mergeString1 + "\" merged with \"" + mergeString2 + "\" is");
		System.out.println("\"" + StaticUtilityMethods.merge(mergeString1, mergeString2) + "\"");
		
		System.out.println(factorizeMe + " have these factors:");
		for(int i : StaticUtilityMethods.integerFactorization(factorizeMe))
			System.out.println(i);
		
		try {
			System.out.println("Sum of all numbers in " + comulativeSum + " is: " + StaticUtilityMethods.recursiveCumulativeSum(comulativeSum));
		} catch (IllegalArgumentException e) {
			System.err.println("Exception: " + e.getLocalizedMessage());
			System.exit(1);
		}
		
		System.exit(0);
	}

}