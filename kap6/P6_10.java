/**
 * 
 */
package kap6;

/**
 * Create, rolls and get values from multiple different dices.
 * Can create without arguments to get 5, 6-sided dice or pass
 * in any amount of different correct sides to create that many
 * of that type of dice.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-10-03
 */
public class P6_10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Yatzy2 yatzy = null;
		try {
			yatzy = new Yatzy2(20, 20, 20, 20);
		} catch (IllegalArgumentException e) {
			System.err.println("Exception: " + e.getLocalizedMessage());
			System.exit(1);
		}
		for(int i = 0; i < 10; i++) {
			yatzy.roll();
			System.out.println(yatzy.toString());
			System.out.println(yatzy.getTotalValue());
		}
	}

}
