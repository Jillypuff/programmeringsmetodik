/**
 * 
 */
package kap2;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Takes an input between 1 000 and 999 999 and gives it back without a space.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-09
 */
public class P2_6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and our variables for the program
		Scanner input = new Scanner(System.in);
		String number = "0";
		Boolean matched = false;
		// Regex to filter input
		Pattern pattern = Pattern.compile("^(0{0,2}[1-9]|0?[1-9][0-9]|[1-9][0-9][0-9]) \\d\\d\\d");
		
		//Filter for an correct input
		while (matched == false) {
			System.out.print("Enter a number between 1 000 and 999 999: ");
			number = input.nextLine();
			Matcher matcher = pattern.matcher(number);
			matched = matcher.matches();
		}
		System.out.format("You have entered: %s", number.replace(" ", ""));
		
		//Clean up and exit
		input.close();
		System.exit(0);
	}
}
