/**
 * 
 */
package kap2;

import java.util.Scanner;

/**
 * Take an input from user and write out that number to the power of 2, 3 and 4.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-07
 */
public class P2_1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Create Scanner for input
		Scanner input = new Scanner(System.in);
		int inputValue = 0;
		
		// Filter incorrect input from user
		System.out.print("Enter a number: ");
		while(true) {
			try {
				inputValue = input.nextInt();
				break;
			} catch (Exception e) {
				System.out.print("Enter a correctly defined int: ");
				input.next();
			}
		}
		
		// Write out the users input to the power of 2, 3 and 4.
		System.out.format("%d ^ 2 = %d\n%d ^ 3 = %d\n%d ^ 4 = %.0f",
				inputValue, (inputValue * inputValue),
				inputValue, (inputValue * inputValue * inputValue),
				inputValue, Math.pow((double) inputValue, 4));
		
		// Clean up and exit.
		input.close();
		System.exit(0);
	}
}
