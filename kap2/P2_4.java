/**
 * 
 */
package kap2;

import java.util.Scanner;

/**
 * Get a radius from user and return area and circumference of a circle with that radius,
 * then the volume and surface of a sphere with same radius.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-08
 */
public class P2_4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and our variables for the program
		Scanner input = new Scanner(System.in);
		double radius = 0;
		boolean checkForCorrectInput = false;
		
		//Filter out non-double or <= 0 inputs
		do {
			System.out.print("Enter a radius: ");
			checkForCorrectInput = input.hasNextDouble();
			if (checkForCorrectInput == true) {
				radius = input.nextDouble();
			} else {
				input.next();
			}
		} while (checkForCorrectInput == false || radius <= 0);
		
		//Calculate and output our data
		double areaOfCircle = Math.PI * Math.pow(radius, 2);
		double circumferenceOfCircle = Math.PI * 2 * radius;
		double volumeOfSphere = Math.PI * 4 * Math.pow(radius, 2);
		double surfaceOfSphere = Math.PI * (4/3) * Math.pow(radius, 3);
		System.out.format("A circle with the radius of %.2f have the area: %.2f, and the circumference: %.2f.\n"
				, radius, areaOfCircle, circumferenceOfCircle);
		System.out.format("A sphere with the radius of %.2f have the volume: %.2f and the surface: %.2f.\n"
				, radius, volumeOfSphere, surfaceOfSphere);
		
		//Clean up and exit
		input.close();
		System.exit(0);
	}
}
