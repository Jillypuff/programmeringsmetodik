/**
 * 
 */
package kap2;

import java.util.Scanner;

/**
 * Take a number as input and output that corresponding month.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-09
 */
public class P2_8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and our variables for the program
		Scanner input = new Scanner(System.in);
		int userInput = 0;
		String months = "January   February  March     April     May       June      "
				+ "July      August    September October   November  December  ";
		
		//Filter input to only a number between 1-12
		do {
			System.out.print("Enter number of a month: ");
			if (input.hasNextInt() == true) {
				userInput = input.nextInt();
			} else
				input.next();
		} while (0 >= userInput || userInput >= 13);
		
		//Outputs month based on the input
		System.out.print(months.substring((userInput-1)*10, (userInput-1)*10 + 10));
		
		//Clean up and exit
		input.close();
		System.exit(0);
	}
}
