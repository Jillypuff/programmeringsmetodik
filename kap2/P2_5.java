/**
 * 
 */
package kap2;

import java.util.Scanner;

/**
 * Takes 2 inputs as base and height of a triangle then calculate
 * and output the area, perimeter and length of the diagonal.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version
 */
public class P2_5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and our variables for the program
		Scanner input = new Scanner(System.in);
		double sideA = 0;
		double sideB = 0;
		boolean checkForCorrectInput = false;
		
		do {
			System.out.print("Enter triangle length: ");
			checkForCorrectInput = input.hasNextDouble();
			if (checkForCorrectInput == true) {
				sideA = input.nextDouble();
			} else {
				input.next();
			}
		} while (checkForCorrectInput == false || sideA <= 0);
		do {
			System.out.print("Enter triangle height: ");
			checkForCorrectInput = input.hasNextDouble();
			if (checkForCorrectInput == true) {
				sideB = input.nextDouble();
			} else {
				input.next();
			}
		} while (checkForCorrectInput == false || sideB <= 0);
		
		//Calculate and output our data
		double areaOfTriangle = (sideA * sideB) / 2;
		double sideC = Math.sqrt((Math.pow(sideA, 2) + (Math.pow(sideB, 2))));
		double perimeterOfTriangle = sideA + sideB + sideC;
		System.out.format("A triangle with length: %.1f and height: %.1f have an area of: %.1f, and a perimeter of %.1f."
				, sideA, sideB, areaOfTriangle, perimeterOfTriangle);
		System.out.format("\nThe final side of the triangle is: %.3f, which we get from the square root of length^2 + height^2."
				, sideC);
		
		//Clean up and exit
		input.close();
		System.exit(0);
	}

}
