/**
 * 
 */
package kap2;

import java.util.Scanner;

/**
 * Take 3 numbers for resistances and uses Ohm's law to calculate total resistance.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-09
 */
public class P2_9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and our variables for the program
		Scanner input = new Scanner(System.in);
		int resistance1 = 0;
		int resistance2 = 0;
		int resistance3 = 0;
		
		//Filter our resistance input to 0 <= integers
		do {
			System.out.print("Enter resistance 1: ");
			if (input.hasNextInt() == true) {
				resistance1 = input.nextInt();
			} else
				input.next();
		} while (resistance1 < 0);
		do {
			System.out.print("Enter resistance 2: ");
			if (input.hasNextInt() == true) {
				resistance2 = input.nextInt();
			} else
				input.next();
		} while (resistance2 < 0);
		do {
			System.out.print("Enter resistance 3: ");
			if (input.hasNextInt() == true) {
				resistance3 = input.nextInt();
			} else
				input.next();
		} while (resistance3 < 0);
		
		//Calculate and print total resistance
		System.out.print("The total resistance is: " + (resistance1 + ((resistance2 * resistance3)/(resistance2 + resistance3))));
		
		//Clean up and exit
		input.close();
		System.exit(0);
	}

}
