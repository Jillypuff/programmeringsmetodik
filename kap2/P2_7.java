/**
 * 
 */
package kap2;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Take in 2 militaryformat times (2359 - 0000) and write out
 * the difference in hours and minutes between the two.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-09
 */
public class P2_7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and our variables for the program
		Scanner input = new Scanner(System.in);
		String time1 = "0000";
		String time2 = "0000";
		Boolean matched = false;
		Pattern pattern = Pattern.compile("^([01]\\d|2[0-3])([0-5]\\d)"); // Regex to filter input
		
        while(matched == false) {
            System.out.print("Enter first time: ");
            time1 = input.nextLine();
            Matcher matcher = pattern.matcher(time1);
            matched = matcher.matches();
        }
        do {
            System.out.print("Enter second time: ");
            time2 = input.nextLine();
            Matcher matcher = pattern.matcher(time2);
            matched = matcher.matches();
        } while (matched == false);
        
        //Put our military string time into total amount of minutes
        int time1Int = (Integer.valueOf(time1.substring(0,2))*60) + (Integer.valueOf(time1.substring(2,4)));
        int time2Int = (Integer.valueOf(time2.substring(0,2))*60) + (Integer.valueOf(time2.substring(2,4)));
        
        //Calculate hours and minute difference
        int hourDifference = (Math.min(Math.abs(time1Int - time2Int) , (24*60 - (Math.abs(time1Int - time2Int)))))/60;
        int minuteDifference = (Math.min(Math.abs(time1Int - time2Int) , (24*60 - (Math.abs(time1Int - time2Int)))))%60;

        System.out.format("Skillnaden på %s och %s är %d timmar och %d minuter.",
        		time1, time2, hourDifference, minuteDifference);


		//Clean up and exit
		input.close();
		System.exit(0);
	}

}
