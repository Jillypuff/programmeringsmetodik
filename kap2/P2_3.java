/**
 * 
 */
package kap2;

import java.util.Scanner;

/**
 * Take input from user that give back multiple fancier outputs based on the input.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-08
 */
public class P2_3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and our variables for the program
		Scanner input = new Scanner(System.in);
		int firstNumber = 0;
		int secondNumber = 0;
		boolean checkForCorrectInput = false;
		
		//Filters out non-int inputs
		do {
			System.out.print("Enter first number: ");
			checkForCorrectInput = input.hasNextInt();
			if(checkForCorrectInput == true) {
				firstNumber = input.nextInt();
			} else {
				input.next();	
			}
		} while(checkForCorrectInput == false);
		
		do {
			System.out.print("Enter second number: ");
			checkForCorrectInput = input.hasNextInt();
			if(checkForCorrectInput == true) {
				secondNumber = input.nextInt();
			} else {
				input.next();	
			}
		} while(checkForCorrectInput == false);
		
		//Gives back formatted data based on the input
		System.out.format("Summa: %11d", 		firstNumber + secondNumber);
		System.out.format("\nDifferens: %7d",	firstNumber - secondNumber);
		System.out.format("\nProdukt: %9d",		firstNumber * secondNumber);
		System.out.format("\nMedel: %14.2f",	(double)(firstNumber + secondNumber) / 2);
		System.out.format("\nDistans: %9d",		Math.abs(firstNumber - secondNumber));
		System.out.format("\nMax: %13d",		Math.max(firstNumber, secondNumber));
		System.out.format("\nMin: %13d",		Math.min(firstNumber, secondNumber));
		
		//Clean up and exit
		input.close();
		System.exit(0);
	}
}
