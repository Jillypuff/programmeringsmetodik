/**
 * 
 */
package kap2;

import java.util.Scanner;

/**
 * Get two particle and a distance from user and use 
 * Coulomb's law to calculate the charge between both particles.
 * @author Jesper Lindberg - JesperLindberg92@protonmail.com
 * @version 2022-09-09
 */
public class P2_10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Create scanner and our variables for the program
		Scanner input = new Scanner(System.in);
		int particle1 = 0;
		int particle2 = 0;
		int distance = 0;
		double electricForce = 0.0;
		
		//Filter outputs to 0 <= integers.
		do {
			System.out.print("Enter charge of particle 1: ");
			if (input.hasNextInt() == true) {
				particle1 = input.nextInt();
			} else
				input.next();
		} while (particle1 <= 0);
		do {
			System.out.print("Enter charge of particle 2: ");
			if (input.hasNextInt() == true) {
				particle2 = input.nextInt();
			} else
				input.next();
		} while (particle2 < 0);
		do {
			System.out.print("Enter distance between particles: ");
			if (input.hasNextInt() == true) {
				distance = input.nextInt();
			} else
				input.next();
		} while (distance <= 0);
		
		//Calculate electric force with Coulomb's law
		final double E = 8.854 * (Math.pow(10, -12));
		electricForce = ((double)particle1 * (double)particle2)/(4 * Math.PI * E * Math.pow(distance, 2));
		System.out.format("The electric force is: %.4f Newton. ", electricForce);
		
		//Clean up and exit
		input.close();
		System.exit(0);
	}

}
